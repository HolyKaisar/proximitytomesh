#include <cassert>
#include "Mesh.h"

namespace ptmDemo
{
	Mesh::Mesh() : m_uVertexCount(0), m_uIndexCount(0), m_uVertexStride(0), m_uVertexOffset(0), m_transform(XMMatrixIdentity())
	{

	}

	Mesh::Mesh(vector<D3DVertex>& vertices, vector<uint32_t>& indices)
	{
		m_vertices = move(vertices);
		m_indices = move(indices);

		m_uVertexCount = m_vertices.size();
		m_uIndexCount = m_indices.size();

		m_uVertexStride = sizeof(D3DVertex);
		m_uVertexOffset = 0;

		m_transform = XMMatrixIdentity();
	}

	Mesh::~Mesh()
	{
		m_vertices.clear();
		m_indices.clear();

		m_uVertexStride = 0;
		m_uVertexOffset = 0;
		m_uIndexCount = 0;
		m_uVertexCount = 0;
	}
}


//#include <cassert>
//#include "Mesh.h"
//
//D3DMesh::D3DMesh() : m_vertexCount(0), m_vertexStride(0), m_vertexOffset(0), m_indicesCount(0), m_worldMat(XMMatrixIdentity())
//{
//	m_pVertexBuffer.Reset();
//	m_pIndexBuffer.Reset();
//	m_pInputLayout.Reset();
//
//	m_pVertexShader.Reset();
//	m_pGeometryShader.Reset();
//	m_pPixelShader.Reset();
//
//	m_pFreqUpdateBuffer.Reset();
//	m_pSamplerLinear.Reset();
//	m_pTransparentBS.Reset();
//
//	m_bTransparent = false;
//
//	m_AABBMin = XMFLOAT3(numeric_limits<float>::max(), numeric_limits<float>::max(), numeric_limits<float>::max());
//	m_AABBMax = XMFLOAT3(numeric_limits<float>::min(), numeric_limits<float>::min(), numeric_limits<float>::min());
//}
//
//D3DMesh::D3DMesh(vector<D3DVertex>& vertices, vector<uint32_t>& indices)
//{
//	D3DMesh();
//
//	m_vertices = move(vertices);
//	m_indices = move(indices);
//
//	m_vertexCount = m_vertices.size();
//	m_indicesCount = m_indices.size();
//
//	m_vertexStride = sizeof(D3DVertex);
//	m_vertexOffset = 0;
//
//	m_worldMat = XMMatrixIdentity();
//
//	m_bTransparent = false;
//
//	CreateBuffers(m_vertices, m_indices);
//}
//
//D3DMesh::~D3DMesh()
//{
//	Destory();
//}
//
//HRESULT D3DMesh::CreateBuffers(vector<D3DVertex>& vertices, vector<uint32_t>& indices)
//{
//	HRESULT hr = S_OK;
//	ID3D11Device* pd3dDevice = DXUTGetD3D11Device();
//
//	// Create Buffer
//	m_pVertexBuffer.Reset();
//	m_pIndexBuffer.Reset();
//	m_pFreqUpdateBuffer.Reset();
//
//	D3D11_BUFFER_DESC bd;
//	ZeroMemory(&bd, sizeof(bd));
//	bd.Usage = D3D11_USAGE_DEFAULT;
//	bd.ByteWidth = sizeof(D3DVertex) * m_vertexCount;
//	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
//	bd.CPUAccessFlags = 0;
//	D3D11_SUBRESOURCE_DATA InitData;
//	ZeroMemory(&InitData, sizeof(InitData));
//	InitData.pSysMem = vertices.data();
//	V_RETURN(pd3dDevice->CreateBuffer(&bd, &InitData, m_pVertexBuffer.GetAddressOf()));
//
//	m_vertexStride = sizeof(D3DVertex);
//	m_vertexOffset = 0;
//
//	bd.Usage = D3D11_USAGE_DEFAULT;
//	bd.ByteWidth = sizeof(DWORD) * m_indicesCount;
//	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
//	bd.CPUAccessFlags = 0;
//	bd.MiscFlags = 0;
//	InitData.pSysMem = indices.data();
//	V_RETURN(pd3dDevice->CreateBuffer(&bd, &InitData, m_pIndexBuffer.GetAddressOf()));
//
//	// Create the constant buffers
//	bd.Usage = D3D11_USAGE_DYNAMIC;
//	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
//	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
//	bd.ByteWidth = sizeof(FreqUpdatedCB);
//	V_RETURN(pd3dDevice->CreateBuffer(&bd, nullptr, m_pFreqUpdateBuffer.GetAddressOf()));
//
//	return hr;
//}
//
//HRESULT D3DMesh::CreateMaterials(wstring shader_file_path)
//{
//	HRESULT hr = S_OK;
//	ID3D11Device* pd3dDevice = DXUTGetD3D11Device();
//
//	DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
//
//#ifdef _DEBUG
//	// Set the D3DCOMPILE_DEBUG flag to embed debug information in the shaders.
//	// Setting this flag improves the shader debugging experience, but still allows 
//	// the shaders to be optimized and to run exactly the way they will run in 
//	// the release configuration of this program.
//	dwShaderFlags |= D3DCOMPILE_DEBUG;
//
//	// Disable optimizations to further improve shader debugging
//	dwShaderFlags |= D3DCOMPILE_SKIP_OPTIMIZATION;
//#endif
//
//	// Compile the vertex shader
//	ID3DBlob* pVSBlob = nullptr;
//	//V_RETURN(DXUTCompileFromFile(L"RefractiveShader.hlsl", nullptr, "VS", "vs_5_0", dwShaderFlags, 0, &pVSBlob));
//	V_RETURN(DXUTCompileFromFile(shader_file_path.c_str(), nullptr, "VS", "vs_5_0", dwShaderFlags, 0, &pVSBlob));
//
//	// Create the vertex shader
//	hr = pd3dDevice->CreateVertexShader(pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), nullptr, m_pVertexShader.GetAddressOf());
//	if (FAILED(hr))
//	{
//		SAFE_RELEASE(pVSBlob);
//		return hr;
//	}
//
//	// Define the input layout
//	D3D11_INPUT_ELEMENT_DESC layout[] =
//	{
//		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
//		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT , D3D11_INPUT_PER_VERTEX_DATA, 0 },
//		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT , D3D11_INPUT_PER_VERTEX_DATA, 0 },
//	};
//	UINT numElements = ARRAYSIZE(layout);
//
//	// Create the input layout
//	hr = pd3dDevice->CreateInputLayout(layout, numElements, pVSBlob->GetBufferPointer(),
//		pVSBlob->GetBufferSize(), &m_pInputLayout);
//	SAFE_RELEASE(pVSBlob);
//	if (FAILED(hr))
//		return hr;
//
//	// Compile the pixel shader
//	ID3DBlob* pPSBlob = nullptr;
//	V_RETURN(DXUTCompileFromFile(shader_file_path.c_str(), nullptr, "PS", "ps_5_0", dwShaderFlags, 0, &pPSBlob));
//
//	// Create the pixel shader
//	hr = pd3dDevice->CreatePixelShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), nullptr, &m_pPixelShader);
//	SAFE_RELEASE(pPSBlob);
//	if (FAILED(hr))
//		return hr;
//
//	// Create the sample state
//	D3D11_SAMPLER_DESC sampDesc;
//	ZeroMemory(&sampDesc, sizeof(sampDesc));
//	sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
//	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
//	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
//	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
//	sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
//	sampDesc.MinLOD = 0;
//	sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
//	V_RETURN(pd3dDevice->CreateSamplerState(&sampDesc, m_pSamplerLinear.GetAddressOf()));
//
//	// Create Blend state
//	m_pTransparentBS.Reset();
//	D3D11_BLEND_DESC blendDesc;
//	ZeroMemory(&blendDesc, sizeof(D3D11_BLEND_DESC));
//	// Create an alpha enabled blend state description.
//	blendDesc.RenderTarget[0].BlendEnable = true;
//	blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
//	blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
//	blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
//	blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
//	blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
//	blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
//	blendDesc.RenderTarget[0].RenderTargetWriteMask = 0x0f;
//	V_RETURN(pd3dDevice->CreateBlendState(&blendDesc, m_pTransparentBS.GetAddressOf()));
//
//	return hr;
//}
//
//void D3DMesh::Destory()
//{
//	m_pVertexBuffer.Reset();
//	m_pIndexBuffer.Reset();
//	m_pInputLayout.Reset();
//
//	m_pVertexShader.Reset();
//	m_pGeometryShader.Reset();
//	m_pPixelShader.Reset();
//
//	m_pFreqUpdateBuffer.Reset();
//	m_pSamplerLinear.Reset();
//	m_pTransparentBS.Reset();
//
//	m_vertexStride = 0;
//	m_vertexOffset = 0;
//	m_indicesCount = 0;
//	m_vertexCount = 0;
//}
//
//
//void D3DMesh::Update(XMMATRIX parentMat, XMMATRIX& view, XMMATRIX& proj)
//{
//	ID3D11DeviceContext* pd3dImmediateContext = DXUTGetD3D11DeviceContext();
//
//	XMMATRIX mWorldViewProjection = m_worldMat * parentMat * view * proj;
//	HRESULT hr;
//	D3D11_MAPPED_SUBRESOURCE mappedResource;
//	V(pd3dImmediateContext->Map(m_pFreqUpdateBuffer.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource));
//	auto pCB = reinterpret_cast<FreqUpdatedCB*>(mappedResource.pData);
//	XMStoreFloat4x4(&pCB->mWorldViewProj, XMMatrixTranspose(mWorldViewProjection));
//	XMStoreFloat4x4(&pCB->mWorld, XMMatrixTranspose(m_worldMat));
//	pCB->vMeshColor = XMFLOAT4(1.0f, 0.0f, 1.0f, 1.0f);
//	pd3dImmediateContext->Unmap(m_pFreqUpdateBuffer.Get(), 0);
//}
//
//
//void D3DMesh::Render()
//{
//	ID3D11DeviceContext* pd3dImmediateContext = DXUTGetD3D11DeviceContext();
//
//	ID3D11RasterizerState* rs = nullptr;
//	pd3dImmediateContext->RSGetState(&rs);
//	float blendFactor[4] = { 0, 0, 0, 0 };
//	if (m_bTransparent) pd3dImmediateContext->OMSetBlendState(m_pTransparentBS.Get(), blendFactor, 0xffffffff);
//
//	// Set the input layout
//	pd3dImmediateContext->IASetInputLayout(m_pInputLayout.Get());
//	// Set vertex buffer/
//	UINT stride = m_vertexStride;
//	UINT offset = m_vertexOffset;
//	pd3dImmediateContext->IASetVertexBuffers(0, 1, m_pVertexBuffer.GetAddressOf(), &stride, &offset);
//	// Set index buffer
//	pd3dImmediateContext->IASetIndexBuffer(m_pIndexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);
//	// Set primitive topology
//	pd3dImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
//
//	pd3dImmediateContext->VSSetShader(m_pVertexShader.Get(), nullptr, 0);
//	pd3dImmediateContext->VSSetConstantBuffers(0, 1, m_pFreqUpdateBuffer.GetAddressOf());
//	pd3dImmediateContext->GSSetShader(m_pGeometryShader.Get(), nullptr, 0);
//	pd3dImmediateContext->GSSetConstantBuffers(0, 1, m_pFreqUpdateBuffer.GetAddressOf());
//	pd3dImmediateContext->PSSetShader(m_pPixelShader.Get(), nullptr, 0);
//	pd3dImmediateContext->PSSetConstantBuffers(0, 1, m_pFreqUpdateBuffer.GetAddressOf());
//
//	pd3dImmediateContext->PSSetSamplers(0, 1, m_pSamplerLinear.GetAddressOf());
//	pd3dImmediateContext->DrawIndexed(m_indicesCount, 0, 0);
//
//	pd3dImmediateContext->OMSetBlendState(0, 0, 0xffffffff);
//	pd3dImmediateContext->RSSetState(rs);
//	SAFE_RELEASE(rs);
//
//	ID3D11VertexShader* tmpVertexShader = nullptr;
//	ID3D11PixelShader* tmpPixelShader = nullptr;
//	ID3D11Buffer* tmpBuffer = nullptr;
//	ID3D11ShaderResourceView* tmpSRV = nullptr;
//	ID3D11SamplerState* tmpSampler = nullptr;
//	pd3dImmediateContext->VSSetShader(tmpVertexShader, nullptr, 0);
//	pd3dImmediateContext->VSSetConstantBuffers(0, 1, &tmpBuffer);
//	pd3dImmediateContext->PSSetShader(tmpPixelShader, nullptr, 0);
//	pd3dImmediateContext->PSSetConstantBuffers(0, 1, &tmpBuffer);
//	pd3dImmediateContext->PSSetShaderResources(0, 1, &tmpSRV);
//	pd3dImmediateContext->PSSetSamplers(0, 1, &tmpSampler);
//}
//
//XMFLOAT3 D3DMesh::GetNearestNeighbor(XMFLOAT3 source)
//{
//	auto dis = [](XMFLOAT3& source, XMFLOAT3& target)
//	{
//		int x(source.x - target.x), y(source.y - target.y), z(source.z - target.z);
//		return sqrt(x * x + y * y + z * z);
//	};
//
//	XMFLOAT3 res(0.0f, 0.0f, 0.0f);
//	float minDis = numeric_limits<float>::max();
//
//	for (auto& v : m_vertices)
//	{
//		float tmpDis = dis(v.mPos, source);
//		if ((minDis - tmpDis) > 0.001)
//		{
//			minDis = tmpDis;
//			res = v.mPos;
//		}
//	}
//	return res;
//}
