#include "ModelLoader.h"
#include "D3DDataStructure.h"
#include "OBJ_Loader.h"
#include "Mesh.h"

namespace ptmDemo
{
	D3DModelLoader::D3DModelLoader()
	{

	}

	D3DModelLoader::~D3DModelLoader()
	{

	}

	shared_ptr<Mesh> D3DModelLoader::LoadObjModel(string model_file_path)
	{
		objl::Loader m_loader;
		bool loadRes = m_loader.LoadFile(model_file_path);
		if (!loadRes)
		{
			cerr << "Failed to load the Obj file from " << model_file_path << endl;
			cerr << __FILE__ << " at line " << __LINE__ << endl;
			return nullptr;
		}

		XMFLOAT3 AABBMin = XMFLOAT3(numeric_limits<float>::max(), numeric_limits<float>::max(), numeric_limits<float>::max());
		XMFLOAT3 AABBMax = XMFLOAT3(numeric_limits<float>::min(), numeric_limits<float>::min(), numeric_limits<float>::min());
		vector<D3DVertex> vertices;
		vector<uint32_t> indices;

		// Assuming each obj has one model
		//assert(m_loader.LoadedMeshes.size() != 1);
		objl::Mesh curMesh = m_loader.LoadedMeshes[0];

		for (auto vex : curMesh.Vertices)
		{
			D3DVertex bv;
			bv.mPos = XMFLOAT3(vex.Position.X, vex.Position.Y, vex.Position.Z);
			bv.mNormal = XMFLOAT3(vex.Normal.X, vex.Normal.Y, vex.Normal.Z);
			bv.mTex = XMFLOAT2(vex.TextureCoordinate.X, vex.TextureCoordinate.Y);
			vertices.push_back(bv);

			AABBMin.x = min(AABBMin.x, bv.mPos.x);
			AABBMin.y = min(AABBMin.y, bv.mPos.y);
			AABBMin.z = min(AABBMin.z, bv.mPos.z);
			AABBMax.x = max(AABBMax.x, bv.mPos.x);
			AABBMax.y = max(AABBMax.y, bv.mPos.y);
			AABBMax.z = max(AABBMax.z, bv.mPos.z);
		}

		float cX = (AABBMax.x + AABBMin.x) / 2.0f;
		float cY = (AABBMax.y + AABBMin.y) / 2.0f;
		float cZ = (AABBMax.z + AABBMin.z) / 2.0f;
		// Move Object Bounding Box Center to the world coordinate origin 
		for (auto& v : vertices)
		{
			v.mPos.x -= cX;
			v.mPos.y -= cY;
			v.mPos.z -= cZ;
		}
		AABBMax.x -= cX, AABBMax.y -= cY, AABBMax.z -= cZ;
		AABBMin.x -= cX, AABBMin.y -= cY, AABBMin.z -= cZ;

		for (int i = 0; i < curMesh.Indices.size(); i += 3)
		{
			indices.push_back(curMesh.Indices[i]);
			indices.push_back(curMesh.Indices[i + 1]);
			indices.push_back(curMesh.Indices[i + 2]);
		}

		shared_ptr<Mesh> newMesh = make_shared<Mesh>(vertices, indices);
		newMesh->m_AABBMax = AABBMax;
		newMesh->m_AABBMin = AABBMin;
		return newMesh;
	}

	shared_ptr<Mesh> D3DModelLoader::LoadOffModel(string model_file_path)
	{
		// Lambda string spliter by delimiter 
		auto stringSpliter = [](string& inputString, char delimiter)
		{
			string token("");
			vector<string> output;
			istringstream iss(inputString);
			while (getline(iss, token, delimiter))
				output.push_back(token);
			return output;
		};

		// Lambda XMFLOAT3 addition
		auto XMFloat3Addition = [](XMFLOAT3& x1, XMFLOAT3& x2)
		{
			return XMFLOAT3(x1.x + x2.x, x1.y + x2.y, x1.z + x2.z);
		};

		ifstream inputOff;
		inputOff.open(model_file_path);
		if (!inputOff)
		{
			cerr << string("Load ") + model_file_path + "is failed" << endl;
			cerr << __FILE__ << " at line " << __LINE__ << endl;
			return nullptr;
		}

		string input_line("");
		getline(inputOff, input_line);
		if (input_line != "OFF")
		{
			cerr << "File Check: Not an off model file." << endl;
			cerr << __FILE__ << " at line " << __LINE__ << endl;
			return nullptr;
		}

		// Load vertices number, faces number, and edges number
		getline(inputOff, input_line);
		vector<string> tmpRes;
		tmpRes = stringSpliter(input_line, ' ');
		if (tmpRes.size() != 3)
		{
			cerr << "Read model infomation failed" << endl;
			cerr << __FILE__ << " at line " << __LINE__ << endl;
			return nullptr;
		}
		auto [numVertices, numFaces, numEdges] = tuple(stoi(tmpRes[0]), stoi(tmpRes[1]), stoi(tmpRes[2]));

		XMFLOAT3 AABBMin = XMFLOAT3(numeric_limits<float>::max(), numeric_limits<float>::max(), numeric_limits<float>::max());
		XMFLOAT3 AABBMax = XMFLOAT3(numeric_limits<float>::min(), numeric_limits<float>::min(), numeric_limits<float>::min());
		vector<D3DVertex> vertices;
		vector<uint32_t> indices;
		// Load Vertex
		for (int i = 0; i < numVertices; i++)
		{
			getline(inputOff, input_line);
			tmpRes = stringSpliter(input_line, ' ');
			if (tmpRes.size() != 3)
			{
				cerr << "Some vertices information is missing." << endl;
				cerr << __FILE__ << " at line " << __LINE__ << endl;
				return nullptr;
			}
			auto [x, y, z] = tuple(stof(tmpRes[0]), stof(tmpRes[1]), stof(tmpRes[2]));
			D3DVertex bv;
			bv.mPos = XMFLOAT3(x, y, z);
			bv.mNormal = XMFLOAT3(0.0f, 0.0f, 0.0f);
			bv.mTex = XMFLOAT2(0.0f, 0.0f);
			vertices.push_back(bv);

			AABBMin.x = min(AABBMin.x, x);
			AABBMin.y = min(AABBMin.y, y);
			AABBMin.z = min(AABBMin.z, z);
			AABBMax.x = max(AABBMax.x, x);
			AABBMax.y = max(AABBMax.y, y);
			AABBMax.z = max(AABBMax.z, z);
		}

		// Move the object to the origin
		float cX = (AABBMax.x + AABBMin.x) / 2.0f;
		float cY = (AABBMax.y + AABBMin.y) / 2.0f;
		float cZ = (AABBMax.z + AABBMin.z) / 2.0f;
		for (auto& v : vertices)
		{
			v.mPos.x -= cX;
			v.mPos.y -= cY;
			v.mPos.z -= cZ;
		}
		AABBMax.x -= cX, AABBMax.y -= cY, AABBMax.z -= cZ;
		AABBMin.x -= cX, AABBMin.y -= cY, AABBMin.z -= cZ;

		// Load faces and calculate normals
		for (int i = 0; i < numFaces; i++)
		{
			getline(inputOff, input_line);
			tmpRes = stringSpliter(input_line, ' ');
			if (tmpRes.size() == 5) // Handle Triangle Strip based on the Triangle Strip format in: https://en.wikipedia.org/wiki/Triangle_strip
			{
				auto [info_num, i1, i2, i3, i4] = tuple(stoi(tmpRes[0]), stoi(tmpRes[1]), stoi(tmpRes[2]), stoi(tmpRes[3]), stoi(tmpRes[4]));
				indices.push_back(i1), indices.push_back(i2), indices.push_back(i3);
				indices.push_back(i3), indices.push_back(i2), indices.push_back(i4);

				// TODO Generate normal fro triangle strip
			}
			else if (tmpRes.size() == 4) // Handle Triangle List
			{
				auto [info_num, i1, i2, i3] = tuple(stoi(tmpRes[0]), stoi(tmpRes[1]), stoi(tmpRes[2]), stoi(tmpRes[3]));
				indices.push_back(i1), indices.push_back(i2), indices.push_back(i3);

				// Generate normal
				XMFLOAT3 normal = GenTriNormal(vertices[i1].mPos, vertices[i2].mPos, vertices[i3].mPos);
				vertices[i1].mNormal = XMFloat3Addition(vertices[i1].mNormal, normal);
				vertices[i2].mNormal = XMFloat3Addition(vertices[i2].mNormal, normal);
				vertices[i3].mNormal = XMFloat3Addition(vertices[i3].mNormal, normal);
			}
			else
			{
				cerr << "Unknown faces format." << endl;
				cerr << __FILE__ << " at line " << __LINE__ << endl;
				return nullptr;
			}
		}

		// Average the normal for each vertex
		for (int i = 0; i < vertices.size(); i++)
			XMStoreFloat3(&vertices[i].mNormal, XMVector3Normalize(XMLoadFloat3(&vertices[i].mNormal)));

		shared_ptr<Mesh> newMesh = make_shared<Mesh>(vertices, indices);
		newMesh->m_AABBMax = AABBMax;
		newMesh->m_AABBMin = AABBMin;
		return newMesh;
	}

	XMFLOAT3  D3DModelLoader::GenTriNormal(XMFLOAT3 v1, XMFLOAT3 v2, XMFLOAT3 v3)
	{
		XMFLOAT3 edge12 = XMFLOAT3(v2.x - v1.x, v2.y - v1.y, v2.z - v1.z);
		XMFLOAT3 edge13 = XMFLOAT3(v3.x - v1.x, v3.y - v1.y, v3.z - v1.z);

		XMVECTOR vec12 = XMLoadFloat3(&edge12);
		XMVECTOR vec23 = XMLoadFloat3(&edge13);
		XMVECTOR normal = XMVector3Cross(vec12, vec23);

		XMFLOAT3 normal_float3;
		XMStoreFloat3(&normal_float3, normal);
		return normal_float3;
	}
}



