#pragma once

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <limits>
#include <cmath>
#include <algorithm>

#include "D3DDataStructure.h"

namespace ptmDemo
{
	using namespace std;
	class Mesh
	{
	public:
		Mesh();
		Mesh(vector<D3DVertex>& vertices, vector<uint32_t>& indices);
		~Mesh();

	public:
		XMMATRIX m_transform;

		XMFLOAT3 m_AABBMin;
		XMFLOAT3 m_AABBMax;

		vector<D3DVertex> m_vertices;
		vector<uint32_t> m_indices;

		uint32_t m_uVertexStride;
		uint32_t m_uVertexOffset;
		uint32_t m_uIndexCount;
		uint32_t m_uVertexCount;
	};
}






//#include "GHeader.h"
//
//using namespace std;
//using namespace DirectX;
//using namespace Microsoft::WRL;
//
//struct D3DVertex
//{
//	XMFLOAT3 mPos;
//	XMFLOAT3 mNormal;
//	XMFLOAT2 mTex;
//};
//
//struct FreqUpdatedCB
//{
//	XMFLOAT4X4 mWorldViewProj;
//	XMFLOAT4X4 mWorld;
//	XMFLOAT4 vMeshColor;
//};
//
//class Mesh
//{
//public:
//	Mesh() : m_transform(XMMatrixIdentity()) {}
//	Mesh(const Mesh& other) = delete;
//	~Mesh() {}
//	
//	virtual void Update(XMMATRIX parentMat, XMMATRIX& view, XMMATRIX& proj) = 0;
//	virtual void Render() = 0;
//	virtual void Destory() = 0;
//	virtual XMFLOAT3 GetNearestNeighbor(XMFLOAT3 source) = 0;
//
//	void SetTransform(XMMATRIX transform)
//	{
//		m_transform = transform;
//	}
//
//	XMMATRIX GetTransform()
//	{
//		return m_transform;
//	}
//
//private:
//	XMMATRIX m_transform;
//
//public:
//	bool m_bTransparent;
//};
//
//class D3DMesh : public Mesh
//{
//public:
//	D3DMesh();
//	D3DMesh(const Mesh& other) = delete;
//	D3DMesh(vector<D3DVertex>& vertices, vector<uint32_t>& indices);
//	~D3DMesh();
//
//	void Update(XMMATRIX parentMat, XMMATRIX& view, XMMATRIX& proj);
//	void Render();
//	void Destory();
//
//	HRESULT CreateBuffers(vector<D3DVertex>& vertices, vector<uint32_t>& indices);
//	HRESULT CreateMaterials(wstring shader_file_path);
//
//
//	uint32_t GetVertexStride() const { return m_vertexStride; }
//	uint32_t GetVertexOffset() const { return m_vertexOffset; }
//	uint32_t GetIndicesCount() const { return m_indicesCount; }
//	uint32_t GetVertexCount()  const { return m_vertexCount; }
//
//	ComPtr<ID3D11Buffer> GetVertexBuffer() { return m_pVertexBuffer; }
//	ComPtr<ID3D11Buffer> GetIndexBuffer() { return m_pIndexBuffer; }
//
//	XMFLOAT3 GetAABBMin() const { return m_AABBMin; }
//	XMFLOAT3 GetAABBMax() const { return m_AABBMax; }
//	void SetAABBMin(XMFLOAT3 aabbMin) { m_AABBMin = aabbMin; }
//	void SetAABBMax(XMFLOAT3 aabbMax) { m_AABBMax = aabbMax; }
//
//	XMFLOAT3 GetNearestNeighbor(XMFLOAT3 source);
//
//private:
//	ComPtr<ID3D11Buffer>		m_pVertexBuffer;
//	ComPtr<ID3D11Buffer>		m_pIndexBuffer;
//	ComPtr<ID3D11InputLayout>	m_pInputLayout;
//
//	ComPtr<ID3D11VertexShader>		m_pVertexShader;
//	ComPtr<ID3D11GeometryShader>	m_pGeometryShader;
//	ComPtr<ID3D11PixelShader>		m_pPixelShader;
//
//	ComPtr<ID3D11Buffer>			m_pFreqUpdateBuffer;
//	ComPtr<ID3D11SamplerState>		m_pSamplerLinear;
//	ComPtr<ID3D11BlendState>		m_pTransparentBS;
//
//	uint32_t					m_vertexStride;
//	uint32_t					m_vertexOffset;
//	uint32_t					m_indicesCount;
//	uint32_t					m_vertexCount;
//
//	XMFLOAT3					m_AABBMin;
//	XMFLOAT3					m_AABBMax;
//
//	XMMATRIX					m_worldMat;
//
//	// Raw data (position, normal, texcoord and indicies)
//	vector<D3DVertex>			m_vertices;
//	vector<uint32_t>			m_indices;
//
//};
