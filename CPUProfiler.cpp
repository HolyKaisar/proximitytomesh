#include "CPUProfiler.h"


/**************************************/
// CPU Info Definition
/**************************************/
CPUInfo::CPUInfo() : m_canReadCpu(false),
m_queryHandle(NULL),
m_counterHandle(NULL),
m_lastSampleTime(0),
m_cpuUsage(0)
{

}

CPUInfo::CPUInfo(const CPUInfo& other) : m_canReadCpu(false),
m_queryHandle(NULL),
m_counterHandle(NULL),
m_lastSampleTime(0),
m_cpuUsage(0)
{

}

CPUInfo::~CPUInfo()
{

}

void CPUInfo::Init()
{
	PDH_STATUS status;

	// Make reading system cpu usage available
	m_canReadCpu = true;

	// Create a query object to poll cpu usage
	status = PdhOpenQuery(NULL, 0, &m_queryHandle);
	if (status != ERROR_SUCCESS)
	{
		m_canReadCpu = false;
		assert(status != ERROR_SUCCESS);
	}

	// Set query object to poll all cpus in the system
	status = PdhAddCounter(m_queryHandle, TEXT("\\Processor(_Total)\\% processor time"), 0, &m_counterHandle);
	if (status != ERROR_SUCCESS)
	{
		m_canReadCpu = false;
		assert(status != ERROR_SUCCESS);
	}

	m_lastSampleTime = GetTickCount64();
	m_cpuUsage = 0;
}

void CPUInfo::Destory()
{
	if (m_canReadCpu)
	{
		PdhCloseQuery(m_queryHandle);
	}
}

void CPUInfo::Update()
{
	PDH_FMT_COUNTERVALUE value;

	if (m_canReadCpu)
	{
		if (m_lastSampleTime + 1000ULL < GetTickCount64())
		{
			m_lastSampleTime = GetTickCount64();
			PdhCollectQueryData(m_queryHandle);
			PdhGetFormattedCounterValue(m_counterHandle, PDH_FMT_LONG, NULL, &value);
			m_cpuUsage = value.longValue;
		}
	}
}

int CPUInfo::GetCpuPercentage()
{
	int usage(0);
	
	if (m_canReadCpu) usage = (int)m_cpuUsage;
	else usage = 0;
	return usage;
}
