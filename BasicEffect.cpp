#include "BasicEffect.h"

BasicEffect::BasicEffect()
{
	m_pVertexShader = nullptr;
	m_pGeometryShader = nullptr;
	m_pPixelShader = nullptr;

	m_pVertexLayout = nullptr;

	m_pFreqUpdateBuffer = nullptr;
	m_pSamplerLinear = nullptr;
}

BasicEffect::~BasicEffect()
{
	DestroyResource();
}

HRESULT BasicEffect::CreateResource(uint32_t bbWidth, uint32_t bbHeight)
{
	HRESULT hr = S_OK;
	ID3D11Device* pd3dDevice = DXUTGetD3D11Device();

	DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;

#ifdef _DEBUG
	// Set the D3DCOMPILE_DEBUG flag to embed debug information in the shaders.
	// Setting this flag improves the shader debugging experience, but still allows 
	// the shaders to be optimized and to run exactly the way they will run in 
	// the release configuration of this program.
	dwShaderFlags |= D3DCOMPILE_DEBUG;

	// Disable optimizations to further improve shader debugging
	dwShaderFlags |= D3DCOMPILE_SKIP_OPTIMIZATION;
#endif

	// Compile the vertex shader
	ID3DBlob* pVSBlob = nullptr;
	V_RETURN(DXUTCompileFromFile(L"BasicEffect.hlsl", nullptr, "VS", "vs_5_0", dwShaderFlags, 0, &pVSBlob));

	// Create the vertex shader
	hr = pd3dDevice->CreateVertexShader(pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), nullptr, m_pVertexShader.GetAddressOf());
	if (FAILED(hr))
	{
		SAFE_RELEASE(pVSBlob);
		return hr;
	}

	// Define the input layout
	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT , D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT , D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	UINT numElements = ARRAYSIZE(layout);

	// Create the input layout
	hr = pd3dDevice->CreateInputLayout(layout, numElements, pVSBlob->GetBufferPointer(),
		pVSBlob->GetBufferSize(), m_pVertexLayout.GetAddressOf());
	SAFE_RELEASE(pVSBlob);
	if (FAILED(hr))
		return hr;

	// Compile the pixel shader
	ID3DBlob* pPSBlob = nullptr;
	V_RETURN(DXUTCompileFromFile(L"BasicEffect.hlsl", nullptr, "PS", "ps_5_0", dwShaderFlags, 0, &pPSBlob));

	// Create the pixel shader
	hr = pd3dDevice->CreatePixelShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), nullptr, m_pPixelShader.GetAddressOf());
	SAFE_RELEASE(pPSBlob);
	if (FAILED(hr))
		return hr;

	// Create the sample state
	D3D11_SAMPLER_DESC sampDesc;
	ZeroMemory(&sampDesc, sizeof(sampDesc));
	sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	//sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	sampDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	sampDesc.MinLOD = 0;
	sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
	V_RETURN(pd3dDevice->CreateSamplerState(&sampDesc, m_pSamplerLinear.GetAddressOf()));

	// Create the constant buffers
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DYNAMIC;
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	bd.ByteWidth = sizeof(FreqUpdatedCB);
	V_RETURN(pd3dDevice->CreateBuffer(&bd, nullptr, m_pFreqUpdateBuffer.GetAddressOf()));

	return hr;
}

void BasicEffect::Render(ComPtr<ID3D11Buffer> vertexBuffer, ComPtr<ID3D11Buffer> indexBuffer, uint32_t indexCnt, uint32_t stride, uint32_t offset,
	XMMATRIX world, XMMATRIX view, XMMATRIX proj)
{
	ID3D11DeviceContext* pd3dImmediateContext = DXUTGetD3D11DeviceContext();


	XMMATRIX mWorldViewProjection = world * view * proj;
	HRESULT hr;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	V(pd3dImmediateContext->Map(m_pFreqUpdateBuffer.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource));
	auto pCB = reinterpret_cast<FreqUpdatedCB*>(mappedResource.pData);
	XMStoreFloat4x4(&pCB->mWorldViewProj, XMMatrixTranspose(mWorldViewProjection));
	XMStoreFloat4x4(&pCB->mWorld, XMMatrixTranspose(world));
	pd3dImmediateContext->Unmap(m_pFreqUpdateBuffer.Get(), 0);


	ID3D11RasterizerState* rs = nullptr;
	pd3dImmediateContext->RSGetState(&rs);

	// Set the input layout
	pd3dImmediateContext->IASetInputLayout(m_pVertexLayout.Get());
	// Set vertex buffer/
	pd3dImmediateContext->IASetVertexBuffers(0, 1, vertexBuffer.GetAddressOf(), &stride, &offset);
	// Set index buffer
	pd3dImmediateContext->IASetIndexBuffer(indexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);
	// Set primitive topology
	pd3dImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);


	pd3dImmediateContext->VSSetShader(m_pVertexShader.Get(), nullptr, 0);
	pd3dImmediateContext->VSSetConstantBuffers(0, 1, m_pFreqUpdateBuffer.GetAddressOf());
	pd3dImmediateContext->PSSetShader(m_pPixelShader.Get(), nullptr, 0);
	pd3dImmediateContext->PSSetSamplers(0, 1, m_pSamplerLinear.GetAddressOf());

	pd3dImmediateContext->DrawIndexed(indexCnt, 0, 0);

	pd3dImmediateContext->OMSetBlendState(0, 0, 0xffffffff);
	pd3dImmediateContext->RSSetState(rs);
	SAFE_RELEASE(rs);

	ID3D11VertexShader* tmpVertexShader = nullptr;
	ID3D11PixelShader* tmpPixelShader = nullptr;
	ID3D11Buffer* tmpBuffer = nullptr;
	ID3D11ShaderResourceView* tmpSRV = nullptr;
	ID3D11SamplerState* tmpSampler = nullptr;
	pd3dImmediateContext->VSSetShader(tmpVertexShader, nullptr, 0);
	pd3dImmediateContext->VSSetConstantBuffers(0, 1, &tmpBuffer);
	pd3dImmediateContext->PSSetShader(tmpPixelShader, nullptr, 0);
	pd3dImmediateContext->PSSetConstantBuffers(0, 1, &tmpBuffer);
	pd3dImmediateContext->PSSetShaderResources(0, 1, &tmpSRV);
	pd3dImmediateContext->PSSetSamplers(0, 1, &tmpSampler);
}

void BasicEffect::DestroyResource()
{
	m_pVertexShader.Reset();
	m_pGeometryShader.Reset();
	m_pPixelShader.Reset();

	m_pVertexLayout.Reset();

	m_pFreqUpdateBuffer.Reset();
	m_pSamplerLinear.Reset();
}
