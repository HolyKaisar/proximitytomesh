#include "Model.h"

namespace ptmDemo
{
	Model::Model() : m_transform(XMMatrixIdentity()), m_name("")
	{

	}

	Model::~Model()
	{
		Destroy();
	}

	bool Model::Create(string model_file_path, string model_name)
	{
		// The valid model file must contain a suffix
		auto it = model_file_path.find('.');
		if (it == string::npos)
		{
			cerr << "Invalid model file." << endl;
			cerr << __FILE__ << " at line " << __LINE__ << endl;
			return false;
		}

		shared_ptr<Mesh> newMesh = nullptr;
		string format = model_file_path.substr(it);
		if (format == ".obj")
		{
			newMesh = m_modelLoader.LoadObjModel(model_file_path);
		}
		else if (format == ".off")
		{
			newMesh = m_modelLoader.LoadOffModel(model_file_path);
		}
		else
		{
			// __FILE__
			// __LINE__
			// __TIME__
			// __DATE__
			// __func__
			cerr << "Model format is not supported." << endl;
			cerr << __FILE__ << " at line " << __LINE__ << endl;
			return false;
		}

		if (newMesh == nullptr)
		{
			cerr << "Model Loading is failed." << endl;
			cerr << __FILE__ << " at line " << __LINE__ << endl;
			return false;
		}

		// Create D3D buffers
		CreateBuffers(newMesh);

		m_name = model_name;
		m_meshes.push_back(newMesh);
		return true;
	}

	void Model::Destroy()
	{
		for (auto& v : m_pVertexBuffers)
			v.Reset();
		for (auto& i : m_pIndexBuffers)
			i.Reset();

		m_meshes.clear();
		m_pVertexBuffers.clear();
		m_pIndexBuffers.clear();
	}

	void Model::Render()
	{
		if (m_meshes.empty()) return;

		ID3D11DeviceContext* pd3dImmediateContext = DXUTGetD3D11DeviceContext();

		// TODO in the future, the model class shoudl merge all its meshes vertex and indices together.
		// For this demo, we have just one mesh for each model

		uint32_t stride = m_meshes[0]->m_uVertexStride;
		uint32_t offset = m_meshes[0]->m_uVertexOffset;

		for (int i = 0; i < m_pVertexBuffers.size(); i++)
		{
			// Set vertex buffer
			pd3dImmediateContext->IASetVertexBuffers(0, 1, m_pVertexBuffers[i].GetAddressOf(), &stride, &offset);
			// Set index buffer
			pd3dImmediateContext->IASetIndexBuffer(m_pIndexBuffers[i].Get(), DXGI_FORMAT_R32_UINT, 0);
		}
	}

	tuple<ComPtr<ID3D11Buffer>, ComPtr< ID3D11Buffer>, uint32_t, uint32_t, uint32_t, uint32_t>
		Model::GetMeshVertexInfo(int meshId)
	{
		if (meshId < 0 or meshId >= m_meshes.size())
		{
			cerr << "Index is out of range." << endl;
			cerr << __FILE__ << " at line " << __LINE__ << endl;
			return tuple(nullptr, nullptr, -1, -1, -1, -1);
		}

		return tuple(m_pVertexBuffers[meshId], m_pIndexBuffers[meshId],
			m_meshes[meshId]->m_uVertexStride, m_meshes[meshId]->m_uVertexOffset,
			m_meshes[meshId]->m_uIndexCount, m_meshes[meshId]->m_uVertexCount);
	}

	XMFLOAT3 Model::FindNearestNeighborForMesh(int meshId, XMFLOAT3 source)
	{
		auto dis = [](XMFLOAT3& source, XMFLOAT3& target)
		{
			int x(source.x - target.x), y(source.y - target.y), z(source.z - target.z);
			return sqrt(x * x + y * y + z * z);
		};

		XMFLOAT3 res(0.0f, 0.0f, 0.0f);
		float minDis = numeric_limits<float>::max();

		for (auto& v : m_meshes[meshId]->m_vertices)
		{
			float tmpDis = dis(v.mPos, source);
			if ((minDis - tmpDis) > 0.001)
			{
				minDis = tmpDis;
				res = v.mPos;
			}
		}
		return res;
	}

	HRESULT Model::CreateBuffers(shared_ptr<Mesh> inputMesh)
	{
		HRESULT hr = S_OK;
		ID3D11Device* pd3dDevice = DXUTGetD3D11Device();

		ComPtr<ID3D11Buffer> vertexBuffer, indexBuffer;
		// Create Buffer
		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(bd));
		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = sizeof(D3DVertex) * inputMesh->m_uVertexCount;
		bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bd.CPUAccessFlags = 0;
		D3D11_SUBRESOURCE_DATA InitData;
		ZeroMemory(&InitData, sizeof(InitData));
		InitData.pSysMem = inputMesh->m_vertices.data();
		V_RETURN(pd3dDevice->CreateBuffer(&bd, &InitData, vertexBuffer.GetAddressOf()));

		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = sizeof(DWORD) * inputMesh->m_uIndexCount;
		bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
		bd.CPUAccessFlags = 0;
		bd.MiscFlags = 0;
		InitData.pSysMem = inputMesh->m_indices.data();
		V_RETURN(pd3dDevice->CreateBuffer(&bd, &InitData, indexBuffer.GetAddressOf()));

		m_pVertexBuffers.push_back(vertexBuffer);
		m_pIndexBuffers.push_back(indexBuffer);
		return hr;
	}
}

//shared_ptr<D3DMesh> Model::D3DLoadOffModel(string model_file_path)
//{
//	// Lambda string spliter by delimiter 
//	auto stringSpliter = [](string& inputString, char delimiter)
//	{
//		string token("");
//		vector<string> output;
//		istringstream iss(inputString);
//		while (getline(iss, token, delimiter))
//			output.push_back(token);
//		return output;
//	};
//
//	// Lambda XMFLOAT3 addition
//	auto XMFloat3Addition = [](XMFLOAT3& x1, XMFLOAT3& x2)
//	{
//		return XMFLOAT3(x1.x + x2.x, x1.y + x2.y, x1.z + x2.z);
//	};
//
//	ifstream inputOff;
//	inputOff.open(model_file_path);
//	if (!inputOff)
//	{
//		cerr << string("Load ") + model_file_path + "is failed" << endl;
//		return nullptr;
//	}
//
//	string input_line("");
//	getline(inputOff, input_line);
//	if (input_line != "OFF")
//	{
//		cerr << "File Check: Not an off model file." << endl;
//		return nullptr;
//	}
//
//	// Load vertices number, faces number, and edges number
//	getline(inputOff, input_line);
//	vector<string> tmpRes;
//	tmpRes = stringSpliter(input_line, ' ');
//	if (tmpRes.size() != 3)
//	{
//		cerr << "Read model infomation failed" << endl;
//		return nullptr;
//	}
//	auto [numVertices, numFaces, numEdges] = tuple(stoi(tmpRes[0]), stoi(tmpRes[1]), stoi(tmpRes[2]));
//
//	XMFLOAT3 AABBMin = XMFLOAT3(numeric_limits<float>::max(), numeric_limits<float>::max(), numeric_limits<float>::max());
//	XMFLOAT3 AABBMax = XMFLOAT3(numeric_limits<float>::min(), numeric_limits<float>::min(), numeric_limits<float>::min());
//	vector<D3DVertex> vertices;
//	vector<uint32_t> indices;
//	// Load Vertex
//	for (int i = 0; i < numVertices; i++)
//	{
//		getline(inputOff, input_line);
//		tmpRes = stringSpliter(input_line, ' ');
//		if (tmpRes.size() != 3)
//		{
//			cerr << "Some vertices information is missing." << endl;
//			return nullptr;
//		}
//		auto [x, y, z] = tuple(stof(tmpRes[0]), stof(tmpRes[1]), stof(tmpRes[2]));
//		D3DVertex bv;
//		bv.mPos = XMFLOAT3(x, y, z);
//		bv.mNormal = XMFLOAT3(0.0f, 0.0f, 0.0f);
//		bv.mTex = XMFLOAT2(0.0f, 0.0f);
//		vertices.push_back(bv);
//
//		AABBMin.x = min(AABBMin.x, x);
//		AABBMin.y = min(AABBMin.y, y);
//		AABBMin.z = min(AABBMin.z, z);
//		AABBMax.x = max(AABBMax.x, x);
//		AABBMax.y = max(AABBMax.y, y);
//		AABBMax.z = max(AABBMax.z, z);
//	}
//
//	// Move the object to the origin
//	float cX = (AABBMax.x + AABBMin.x) / 2.0f;
//	float cY = (AABBMax.y + AABBMin.y) / 2.0f;
//	float cZ = (AABBMax.z + AABBMin.z) / 2.0f;
//	for (auto& v : vertices)
//	{
//		v.mPos.x -= cX;
//		v.mPos.y -= cY;
//		v.mPos.z -= cZ;
//	}
//	AABBMax.x -= cX, AABBMax.y -= cY, AABBMax.z -= cZ;
//	AABBMin.x -= cX, AABBMin.y -= cY, AABBMin.z -= cZ;
//
//	// Load faces and calculate normals
//	for (int i = 0; i < numFaces; i++)
//	{
//		getline(inputOff, input_line);
//		tmpRes = stringSpliter(input_line, ' ');
//		if (tmpRes.size() == 5) // Handle Triangle Strip based on the Triangle Strip format in: https://en.wikipedia.org/wiki/Triangle_strip
//		{
//			auto [info_num, i1, i2, i3, i4] = tuple(stoi(tmpRes[0]), stoi(tmpRes[1]), stoi(tmpRes[2]), stoi(tmpRes[3]), stoi(tmpRes[4]));
//			indices.push_back(i1), indices.push_back(i2), indices.push_back(i3);
//			indices.push_back(i3), indices.push_back(i2), indices.push_back(i4);
//
//			// TODO Generate normal fro triangle strip
//		}
//		else if (tmpRes.size() == 4) // Handle Triangle List
//		{
//			auto [info_num, i1, i2, i3] = tuple(stoi(tmpRes[0]), stoi(tmpRes[1]), stoi(tmpRes[2]), stoi(tmpRes[3]));
//			indices.push_back(i1), indices.push_back(i2), indices.push_back(i3);
//
//			// Generate normal
//			XMFLOAT3 v1 = vertices[i1].mPos, v2 = vertices[i2].mPos, v3 = vertices[i3].mPos;
//			XMFLOAT3 edge12 = XMFLOAT3(v2.x - v1.x, v2.y - v1.y, v2.z - v1.z);
//			XMFLOAT3 edge13 = XMFLOAT3(v3.x - v1.x, v3.y - v1.y, v3.z - v1.z);
//
//			XMVECTOR vec12 = XMLoadFloat3(&edge12);
//			XMVECTOR vec23 = XMLoadFloat3(&edge13);
//			XMVECTOR normal = XMVector3Cross(vec12, vec23);
//
//			XMFLOAT3 normal_float3;
//			XMStoreFloat3(&normal_float3, normal);
//			vertices[i1].mNormal = XMFloat3Addition(vertices[i1].mNormal, normal_float3);
//			vertices[i2].mNormal = XMFloat3Addition(vertices[i2].mNormal, normal_float3);
//			vertices[i3].mNormal = XMFloat3Addition(vertices[i3].mNormal, normal_float3);
//		}
//		else
//		{
//			cerr << "Unknown faces format." << endl;
//			return nullptr;
//		}
//	}
//
//	// Average the normal for each vertex
//	for (int i = 0; i < vertices.size(); i++)
//		XMStoreFloat3(&vertices[i].mNormal, XMVector3Normalize(XMLoadFloat3(&vertices[i].mNormal)));
//
//	shared_ptr<D3DMesh> newD3DMesh = make_shared<D3DMesh>(vertices, indices);
//	newD3DMesh->SetAABBMax(AABBMax);
//	newD3DMesh->SetAABBMin(AABBMin);
//
//	return newD3DMesh;
//}

//shared_ptr<D3DMesh> Model::D3DLoadObjModel(string model_file_path)
//{
	//objl::Loader m_loader;
	//bool loadRes = m_loader.LoadFile(model_file_path);
	////assert(loadRes == false);
	//if (!loadRes) return nullptr;

	//XMFLOAT3 AABBMin = XMFLOAT3(numeric_limits<float>::max(), numeric_limits<float>::max(), numeric_limits<float>::max());
	//XMFLOAT3 AABBMax = XMFLOAT3(numeric_limits<float>::min(), numeric_limits<float>::min(), numeric_limits<float>::min());
	//vector<D3DVertex> vertices;
	//vector<uint32_t> indices;

	//// Assuming each obj has one model
	////assert(m_loader.LoadedMeshes.size() != 1);
	//objl::Mesh curMesh = m_loader.LoadedMeshes[0];

	//for (auto vex : curMesh.Vertices)
	//{
	//	D3DVertex bv;
	//	bv.mPos = XMFLOAT3(vex.Position.X, vex.Position.Y, vex.Position.Z);
	//	bv.mNormal = XMFLOAT3(vex.Normal.X, vex.Normal.Y, vex.Normal.Z);
	//	bv.mTex = XMFLOAT2(vex.TextureCoordinate.X, vex.TextureCoordinate.Y);
	//	vertices.push_back(bv);

	//	AABBMin.x = min(AABBMin.x, bv.mPos.x);
	//	AABBMin.y = min(AABBMin.y, bv.mPos.y);
	//	AABBMin.z = min(AABBMin.z, bv.mPos.z);
	//	AABBMax.x = max(AABBMax.x, bv.mPos.x);
	//	AABBMax.y = max(AABBMax.y, bv.mPos.y);
	//	AABBMax.z = max(AABBMax.z, bv.mPos.z);
	//}

	//float cX = (AABBMax.x + AABBMin.x) / 2.0f;
	//float cY = (AABBMax.y + AABBMin.y) / 2.0f;
	//float cZ = (AABBMax.z + AABBMin.z) / 2.0f;
	//// Move Object Bounding Box Center to the world coordinate origin 
	//for (auto& v : vertices)
	//{
	//	v.mPos.x -= cX;
	//	v.mPos.y -= cY;
	//	v.mPos.z -= cZ;
	//}
	//AABBMax.x -= cX, AABBMax.y -= cY, AABBMax.z -= cZ;
	//AABBMin.x -= cX, AABBMin.y -= cY, AABBMin.z -= cZ;

	//for (int i = 0; i < curMesh.Indices.size(); i += 3)
	//{
	//	indices.push_back(curMesh.Indices[i]);
	//	indices.push_back(curMesh.Indices[i + 1]);
	//	indices.push_back(curMesh.Indices[i + 2]);
	//}

	//shared_ptr<D3DMesh> newD3DMesh = make_shared<D3DMesh>(vertices, indices);
	//newD3DMesh->SetAABBMax(AABBMax);
	//newD3DMesh->SetAABBMin(AABBMin);

	//return newD3DMesh;
//	return nullptr;
//}
