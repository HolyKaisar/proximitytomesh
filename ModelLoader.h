#pragma once

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <limits>
#include <cmath>
#include <algorithm>

#include <DirectXMath.h>



namespace ptmDemo
{
	using namespace std;
	using namespace DirectX;

	class Mesh;

	class D3DModelLoader
	{
	public:
		D3DModelLoader();
		~D3DModelLoader();

		shared_ptr<Mesh> LoadObjModel(string model_file_path);
		shared_ptr<Mesh> LoadOffModel(string model_file_path);

	private:
		XMFLOAT3 GenTriNormal(XMFLOAT3 p1, XMFLOAT3 p2, XMFLOAT3 p3);
	};
}

