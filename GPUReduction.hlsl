/* A low efficient version of texture reduction */

Texture2D<float4> ReductionInput: register(t0);

cbuffer cbAllInOne : register(b0) {
	int kernelW;
	int kernelH;
	int currentW;
	int currentH;
}

struct VS_INPUT {
	float3 Pos : POSITION;
	float2 Tex : TEXCOORD;
};

struct GS_INPUT {};

struct PS_INPUT {
	float4 Pos : SV_POSITION;
	float2 Tex : TEXCOORD0;
};

PS_INPUT VS(VS_INPUT input) {
	PS_INPUT output = (PS_INPUT)0;
	output.Pos = float4(input.Pos, 1.0);
	output.Tex = input.Tex;
	return output;
}

float4 PS(PS_INPUT input) : SV_Target{
	uint width = 0, height = 0;
	ReductionInput.GetDimensions(width, height);

	int2 coords = input.Tex * float2(currentW, currentH);
	coords = coords * int2(kernelW, kernelH);

	float minV = 1e9;
	float4 res = float4(0.0f, 0.0f, 0.0f, 0.0f);
	[unroll]
	for (uint i = 0; i < 4; i++)
		[unroll]
		for (uint j = 0; j < 4; j++)
		{
			float4 elem = ReductionInput.Load(int3(coords + int2(i, j), 0));
			if (elem.z * 10000 < minV && (elem.z * 10000 - 0.0) > 0.001)
			{
				minV = elem.z * 10000;
				res = elem;
			}
		}
	return res;
}

//// Brute Force
//float4 PS_BruteForce(PS_INPUT input) : SV_Target{
//	uint width = 0, height = 0;
//	ReductionInput.GetDimensions(width, height);
//
//	int2 coords = input.Tex * iOutputCurrentSubRegRes;
//	int2 originalSubRegRes = (uint)iOutputOriginalSubRegRes / (uint)iChildrenNums;
//	int2 startPoint = coords * originalSubRegRes;
//
//	float sum = 0;
//	//[unroll]
//	for (uint i = 0; i < originalSubRegRes.x; i++)
//		//[unroll]
//		for (uint j = 0; j < originalSubRegRes.y; j++)
//		{
//			sum += ReductionInput.Load(int3(startPoint + int2(i, j), 0));
//		}
//	return sum;
//	//return startPoint.x + startPoint.y;
//}