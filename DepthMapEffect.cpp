#include "DepthMapEffect.h"

DepthMapEffect::DepthMapEffect()
{
	m_pVertexShaderDepth = nullptr;
	m_pGeometryShaderDepth = nullptr;
	m_pPixelShaderDepth = nullptr;

	m_pTex2DDepth = nullptr;
	m_pSRVDepth = nullptr;
	m_pRTVDepth = nullptr;

	m_pVertexLayout = nullptr;

	m_pFreqUpdateBufferDepth = nullptr;

	m_pDepthTex2D = nullptr;
	m_pDepthStencilState = nullptr;
	m_pDepthStencilView = nullptr;
}

DepthMapEffect::~DepthMapEffect()
{
	DestroyResource();
}

HRESULT DepthMapEffect::CreateResource(uint32_t bbWidth, uint32_t bbHeight)
{
	HRESULT hr = S_OK;
	ID3D11Device* pd3dDevice = DXUTGetD3D11Device();

	DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;

#ifdef _DEBUG
	// Set the D3DCOMPILE_DEBUG flag to embed debug information in the shaders.
	// Setting this flag improves the shader debugging experience, but still allows 
	// the shaders to be optimized and to run exactly the way they will run in 
	// the release configuration of this program.
	dwShaderFlags |= D3DCOMPILE_DEBUG;

	// Disable optimizations to further improve shader debugging
	dwShaderFlags |= D3DCOMPILE_SKIP_OPTIMIZATION;
#endif

	// Compile the vertex shader
	ID3DBlob* pVSBlob = nullptr;
	V_RETURN(DXUTCompileFromFile(L"DepthMap.hlsl", nullptr, "VS", "vs_5_0", dwShaderFlags, 0, &pVSBlob));

	// Create the vertex shader
	hr = pd3dDevice->CreateVertexShader(pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), nullptr, m_pVertexShaderDepth.GetAddressOf());
	if (FAILED(hr))
	{
		SAFE_RELEASE(pVSBlob);
		return hr;
	}

	// Define the input layout
	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT , D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT , D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	UINT numElements = ARRAYSIZE(layout);

	// Create the input layout
	hr = pd3dDevice->CreateInputLayout(layout, numElements, pVSBlob->GetBufferPointer(),
		pVSBlob->GetBufferSize(), m_pVertexLayout.GetAddressOf());
	SAFE_RELEASE(pVSBlob);
	if (FAILED(hr))
		return hr;

	// Compile the pixel shader
	ID3DBlob* pPSBlob = nullptr;
	V_RETURN(DXUTCompileFromFile(L"DepthMap.hlsl", nullptr, "PS", "ps_5_0", dwShaderFlags, 0, &pPSBlob));

	// Create the pixel shader
	hr = pd3dDevice->CreatePixelShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), nullptr, m_pPixelShaderDepth.GetAddressOf());
	SAFE_RELEASE(pPSBlob);
	if (FAILED(hr))
		return hr;

	// Create the constant buffers
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DYNAMIC;
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	//bd.CPUAccessFlags = 0;
	bd.ByteWidth = sizeof(FreqUpdatedCBDepth);
	V_RETURN(pd3dDevice->CreateBuffer(&bd, nullptr, m_pFreqUpdateBufferDepth.GetAddressOf()));


	// Create texture resources
	D3D11_TEXTURE2D_DESC	RTtextureDesc = { 0 };
	RTtextureDesc.Width = bbWidth;
	RTtextureDesc.Height = bbHeight;
	RTtextureDesc.MipLevels = 1;
	RTtextureDesc.ArraySize = 1;
	RTtextureDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	RTtextureDesc.SampleDesc.Count = 1;
	RTtextureDesc.Usage = D3D11_USAGE_DEFAULT;
	RTtextureDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	RTtextureDesc.CPUAccessFlags = 0;
	RTtextureDesc.MiscFlags = 0;
	V_RETURN(pd3dDevice->CreateTexture2D(&RTtextureDesc, NULL, m_pTex2DDepth.GetAddressOf()));

	D3D11_SHADER_RESOURCE_VIEW_DESC RTshaderResourceDesc;
	RTshaderResourceDesc.Format = RTtextureDesc.Format;
	RTshaderResourceDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	RTshaderResourceDesc.Texture2D.MostDetailedMip = 0;
	RTshaderResourceDesc.Texture2D.MipLevels = 1;
	V_RETURN(pd3dDevice->CreateShaderResourceView(m_pTex2DDepth.Get(), &RTshaderResourceDesc, m_pSRVDepth.GetAddressOf()));

	D3D11_RENDER_TARGET_VIEW_DESC	RTviewDesc;
	RTviewDesc.Format = RTtextureDesc.Format;
	RTviewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	RTviewDesc.Texture2D.MipSlice = 0;
	V_RETURN(pd3dDevice->CreateRenderTargetView(m_pTex2DDepth.Get(), &RTviewDesc, m_pRTVDepth.GetAddressOf()));

	// Create Stencil View
	D3D11_TEXTURE2D_DESC descDepth;
	ZeroMemory(&descDepth, sizeof(descDepth));
	descDepth.Width = bbWidth;
	descDepth.Height = bbHeight;
	descDepth.MipLevels = 1;
	descDepth.ArraySize = 1;
	descDepth.Format = DXUTGetDeviceSettings().d3d11.AutoDepthStencilFormat;
	descDepth.SampleDesc.Count = 1;
	descDepth.SampleDesc.Quality = 0;
	descDepth.Usage = D3D11_USAGE_DEFAULT;
	descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	descDepth.CPUAccessFlags = 0;
	descDepth.MiscFlags = 0;
	hr = pd3dDevice->CreateTexture2D(&descDepth, NULL, m_pDepthTex2D.GetAddressOf());

	D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
	ZeroMemory(&descDSV, sizeof(descDSV));
	descDSV.Format = descDepth.Format;
	descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	descDSV.Texture2D.MipSlice = 0;

	// Create Depth Stencil State
	D3D11_DEPTH_STENCIL_DESC dsDesc;
	// Depth test parameters
	dsDesc.DepthEnable = true; // Disable Depth Test
	dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	//dsDesc.DepthFunc = D3D11_COMPARISON_LESS;
	dsDesc.DepthFunc = D3D11_COMPARISON_LESS;
	// Stencil test parameters
	dsDesc.StencilEnable = true;
	dsDesc.StencilReadMask = 0xFF;
	dsDesc.StencilWriteMask = 0xFF;
	// Stencil operations if pixel is front-facing
	dsDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	dsDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	// Stencil operations if pixel is back-facing
	dsDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	dsDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	V_RETURN(pd3dDevice->CreateDepthStencilState(&dsDesc, m_pDepthStencilState.GetAddressOf()));

	// Create the depth stencil view
	V_RETURN(pd3dDevice->CreateDepthStencilView(m_pDepthTex2D.Get(), // Depth stencil texture
		&descDSV, // Depth stencil desc
		m_pDepthStencilView.GetAddressOf()));

	m_ViewPort.TopLeftX = 0;
	m_ViewPort.TopLeftY = 0;
	m_ViewPort.MinDepth = 0.0f;
	m_ViewPort.MaxDepth = 1.0f;
	m_ViewPort.Width = static_cast<float>(bbWidth);
	m_ViewPort.Height = static_cast<float>(bbHeight);

	return hr;
}


void DepthMapEffect::RenderDepthMap(ComPtr<ID3D11Buffer> vertexBuffer, ComPtr<ID3D11Buffer> indexBuffer, uint32_t indexCnt, uint32_t stride, uint32_t offset,
	XMMATRIX world, XMMATRIX view, XMMATRIX proj)
{
	ID3D11DeviceContext* pd3dImmediateContext = DXUTGetD3D11DeviceContext();

	XMMATRIX mWorldViewProjection = world * view * proj;
	HRESULT hr;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	V(pd3dImmediateContext->Map(m_pFreqUpdateBufferDepth.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource));
	auto pCB = reinterpret_cast<FreqUpdatedCBDepth*>(mappedResource.pData);
	XMStoreFloat4x4(&pCB->mWorldViewProj, XMMatrixTranspose(mWorldViewProjection));
	pd3dImmediateContext->Unmap(m_pFreqUpdateBufferDepth.Get(), 0);


	ID3D11RasterizerState* rs = nullptr;
	pd3dImmediateContext->RSGetState(&rs);

	pd3dImmediateContext->ClearRenderTargetView(m_pRTVDepth.Get(), Colors::Black);
	pd3dImmediateContext->ClearDepthStencilView(m_pDepthStencilView.Get(), D3D11_CLEAR_DEPTH, 1.0, 0);
	pd3dImmediateContext->OMSetRenderTargets(1, m_pRTVDepth.GetAddressOf(), m_pDepthStencilView.Get());
	pd3dImmediateContext->OMSetDepthStencilState(m_pDepthStencilState.Get(), 1);

	pd3dImmediateContext->RSSetViewports(1, &m_ViewPort);

	// Set the input layout
	pd3dImmediateContext->IASetInputLayout(m_pVertexLayout.Get());
	// Set vertex buffer/
	pd3dImmediateContext->IASetVertexBuffers(0, 1, vertexBuffer.GetAddressOf(), &stride, &offset);
	// Set index buffer
	pd3dImmediateContext->IASetIndexBuffer(indexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);
	// Set primitive topology
	pd3dImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);


	pd3dImmediateContext->VSSetShader(m_pVertexShaderDepth.Get(), nullptr, 0);
	pd3dImmediateContext->VSSetConstantBuffers(0, 1, m_pFreqUpdateBufferDepth.GetAddressOf());
	pd3dImmediateContext->PSSetShader(m_pPixelShaderDepth.Get(), nullptr, 0);

	pd3dImmediateContext->DrawIndexed(indexCnt, 0, 0);

	pd3dImmediateContext->OMSetBlendState(0, 0, 0xffffffff);
	pd3dImmediateContext->RSSetState(rs);
	SAFE_RELEASE(rs);

	ID3D11VertexShader* tmpVertexShader = nullptr;
	ID3D11PixelShader* tmpPixelShader = nullptr;
	ID3D11Buffer* tmpBuffer = nullptr;
	ID3D11ShaderResourceView* tmpSRV = nullptr;
	ID3D11SamplerState* tmpSampler = nullptr;
	pd3dImmediateContext->VSSetShader(tmpVertexShader, nullptr, 0);
	pd3dImmediateContext->VSSetConstantBuffers(0, 1, &tmpBuffer);
	pd3dImmediateContext->PSSetShader(tmpPixelShader, nullptr, 0);
	pd3dImmediateContext->PSSetConstantBuffers(0, 1, &tmpBuffer);
	pd3dImmediateContext->PSSetShaderResources(0, 1, &tmpSRV);
	pd3dImmediateContext->PSSetSamplers(0, 1, &tmpSampler);
}

void DepthMapEffect::DestroyResource()
{
	m_pVertexShaderDepth.Reset();
	m_pGeometryShaderDepth.Reset();
	m_pPixelShaderDepth.Reset();

	m_pDepthTex2D.Reset();
	m_pDepthStencilState.Reset();
	m_pDepthStencilView.Reset();

	m_pTex2DDepth.Reset();
	m_pSRVDepth.Reset();
	m_pRTVDepth.Reset();

	m_pVertexLayout.Reset();

	m_pFreqUpdateBufferDepth.Reset();
}

