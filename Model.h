#pragma once

#include "GHeader.h"
#include "Mesh.h"
#include "ModelLoader.h"

namespace ptmDemo
{
	class Model
	{
	public:
		Model();
		~Model();

		bool Create(string model_file_path, string model_name);
		void Render();
		void Destroy();


		void SetTranslation(XMMATRIX newTrans) 
		{ 
			m_transform = m_transform * newTrans; 
		}
		XMFLOAT4 GetTranslation()
		{
			XMVECTOR scale, rotQuad, trans;
			XMMatrixDecompose(&scale, &rotQuad, &trans, m_transform);
			XMFLOAT4 res;
			XMStoreFloat4(&res, trans);
			return res;
		}

		void SetRotation(XMMATRIX newRot) 
		{ 
			m_transform = m_transform * newRot; 
		}
		XMFLOAT4 GetRotationQuad()
		{
			XMVECTOR scale, rotQuad, trans;
			XMMatrixDecompose(&scale, &rotQuad, &trans, m_transform);
			XMFLOAT4 res;
			XMStoreFloat4(&res, rotQuad);
			return res;
		}

		void SetTransform(XMMATRIX newTransform)
		{
			m_transform = newTransform;
		}
		XMMATRIX GetTranform()
		{
			return m_transform;
		}

		tuple<ComPtr<ID3D11Buffer>, ComPtr< ID3D11Buffer>, uint32_t, uint32_t, uint32_t, uint32_t> GetMeshVertexInfo(int meshId);

		XMFLOAT3 FindNearestNeighborForMesh(int meshId, XMFLOAT3 source);

	private:
		HRESULT CreateBuffers(shared_ptr<Mesh> inputMesh);

	private:
		XMMATRIX m_transform;

		vector<ComPtr<ID3D11Buffer>> m_pVertexBuffers;
		vector<ComPtr<ID3D11Buffer>> m_pIndexBuffers;
	private:
		vector<shared_ptr<Mesh>> m_meshes;
		string m_name;

		D3DModelLoader m_modelLoader;
	};
}
