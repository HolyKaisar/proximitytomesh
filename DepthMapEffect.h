#pragma once
#include "GHeader.h"

class DepthMapEffect
{
public:
	DepthMapEffect();
	~DepthMapEffect();

	HRESULT CreateResource(uint32_t bbWidth, uint32_t bbHeight);
	void	DestroyResource();
	void	RenderDepthMap(ComPtr<ID3D11Buffer> vertexBuffer, ComPtr<ID3D11Buffer> indexBuffer, uint32_t indexCnt,
		uint32_t stride, uint32_t offset, XMMATRIX world, XMMATRIX view, XMMATRIX proj);

	ComPtr<ID3D11ShaderResourceView> GetDepthMap()
	{
		return m_pSRVDepth;
	}

private:
	ComPtr<ID3D11VertexShader>		m_pVertexShaderDepth;
	ComPtr<ID3D11GeometryShader>	m_pGeometryShaderDepth;
	ComPtr<ID3D11PixelShader>		m_pPixelShaderDepth;

	ComPtr<ID3D11Texture2D>				m_pTex2DDepth;
	ComPtr<ID3D11ShaderResourceView>	m_pSRVDepth;
	ComPtr<ID3D11RenderTargetView>		m_pRTVDepth;

	ComPtr<ID3D11Texture2D>				m_pDepthTex2D;
	ComPtr<ID3D11DepthStencilState>		m_pDepthStencilState;
	ComPtr<ID3D11DepthStencilView>		m_pDepthStencilView;

	ComPtr<ID3D11Buffer>			m_pFreqUpdateBufferDepth;

	ComPtr<ID3D11InputLayout>		m_pVertexLayout;

	D3D11_VIEWPORT					m_ViewPort;

	uint32_t						m_bbWidth;
	uint32_t						m_bbHeight;

private:
	struct FreqUpdatedCBDepth
	{
		XMFLOAT4X4 mWorldViewProj;
	};

	struct FreqUpdatedCBShadowMap
	{
		XMFLOAT4X4 mWorldViewProj;
		XMFLOAT4X4 mWorldViewProj2;
		XMFLOAT4X4 mWorld;
	};
};
