#pragma once

#include "GHeader.h"
#include "GPUTexReduction.h"

class FindNearestNeighbor
{
public:
	FindNearestNeighbor();
	~FindNearestNeighbor();
	
	HRESULT CreateResource(ID3D11Device* pd3dDevice, uint32_t bbWidth, uint32_t bbHeight);
	void    DestroyResource();
	void	GenerateDisMap(ComPtr<ID3D11Buffer> pVertexBuffer, ComPtr<ID3D11Buffer> pIndicesBuffer,
							uint32_t stride, uint32_t offset, uint32_t indicesCnt);
	void	GetNearestNeighbor();
	void	Update(XMMATRIX& world, XMMATRIX& view, XMMATRIX& proj, XMFLOAT4 ctrPt);

	ID3D11ShaderResourceView** GetFNNResulttSRV() { return m_pSRV.GetAddressOf(); }

private:
	ComPtr<ID3D11VertexShader>		m_pVertexShader;
	ComPtr<ID3D11PixelShader>		m_pPixelShader;
	ComPtr<ID3D11InputLayout>		m_pVertexLayout;
	ComPtr<ID3D11SamplerState>		m_pSamplerLinear;

	ComPtr<ID3D11Buffer>			m_pFreqUpdateBuffer;

	ComPtr<ID3D11Texture2D>				m_pTex2D;
	ComPtr<ID3D11ShaderResourceView>	m_pSRV;
	ComPtr<ID3D11RenderTargetView>		m_pRTV;

	ComPtr<ID3D11Texture2D>			m_pDepthTex2D;
	ComPtr<ID3D11DepthStencilState> m_pDepthStencilState;
	ComPtr<ID3D11DepthStencilView>	m_pDepthStencilView;

	D3D11_VIEWPORT				m_ViewPort;

	uint32_t					m_bbWidth;
	uint32_t					m_bbHeight;

	GPUTexReduction				m_texReduc;

private:
	struct FreqUpdatedCB
	{
		XMFLOAT4X4 mWorldViewProj;
		XMFLOAT4X4 mWorld;
		XMFLOAT4   mPointPos;
	};
};

