
//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
SamplerState samLinear : register(s0);

cbuffer cbChangesEveryFrame : register(b0)
{
	matrix WorldViewProj;
	matrix World;
	float4 vMeshColor;
};

//--------------------------------------------------------------------------------------
struct VS_INPUT
{
	float3 Pos : POSITION;
	float3 Nor : NORMAL;
	float2 Tex : TEXCOORD;
};

struct PS_INPUT
{
	float4 Pos : SV_POSITION;
	float3 Nor : NORMAL;
	float2 Tex : TEXCOORD0;
};


//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
PS_INPUT VS(VS_INPUT input)
{
	PS_INPUT output = (PS_INPUT)0;
	output.Pos = mul(float4(input.Pos, 1.0f), WorldViewProj);
	output.Nor = input.Nor;
	output.Tex = input.Tex;

	return output;
}


//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(PS_INPUT input) : SV_Target
{
	//return txDiffuse.Sample(samLinear, input.Tex) * vMeshColor;

	float3 lightPos = float3(0.0f, 4.0f, 5.0f);
	//lightPos = vCameraPos.xyz;
	float3 lightDir = float3(0.0f, 0.0f, 0.0f) - lightPos;
	float3 lightD = -lightDir;
	lightD = normalize(lightD);

	float lightIntensity = saturate(dot(normalize(input.Nor), lightD));

	float4 color = float4(0.15f, 0.15f, 0.15f, 1.0f);
	float4 diffuseColor = float4(0.5f, 0.3f, 0.6f, 1.0f);
	if (lightIntensity > 0.0f)
	{
		// Determine the final diffuse color based on the diffuse color and the amount of light intensity.
		color += (diffuseColor * lightIntensity);
	}

	// TODO may need to add some lighting here; Phone Model First then a Unreal BRDF implementation
	color.a = 0.1f;
	return saturate(color);
}
