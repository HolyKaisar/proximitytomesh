#pragma once

#include "GHeader.h"
#include "D3DDataStructure.h"

class BasicEffect
{
public:
	BasicEffect();
	~BasicEffect();

	HRESULT CreateResource(uint32_t bbWidth, uint32_t bbHeight);
	void DestroyResource();
	void Render(ComPtr<ID3D11Buffer> vertexBuffer, ComPtr<ID3D11Buffer> indexBuffer, uint32_t indexCnt, uint32_t stride, uint32_t offset,
		XMMATRIX world, XMMATRIX view, XMMATRIX proj);

private:
	ComPtr<ID3D11VertexShader>		m_pVertexShader;
	ComPtr<ID3D11GeometryShader>	m_pGeometryShader;
	ComPtr<ID3D11PixelShader>		m_pPixelShader;

	ComPtr<ID3D11InputLayout>		m_pVertexLayout;

	ComPtr<ID3D11Buffer>			m_pFreqUpdateBuffer;
	ComPtr<ID3D11SamplerState>		m_pSamplerLinear;

	D3D11_VIEWPORT					m_ViewPort;

	uint32_t						m_bbWidth;
	uint32_t						m_bbHeight;

private:
	struct FreqUpdatedCB
	{
		XMFLOAT4X4 mWorldViewProj;
		XMFLOAT4X4 mWorld;
	};
};

