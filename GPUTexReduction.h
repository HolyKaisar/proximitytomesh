#pragma once
#include "GHeader.h"

class GPUTexReduction
{
public:
	GPUTexReduction();
	~GPUTexReduction();

	HRESULT CreateResource(ID3D11Device* pd3dDevice, uint32_t bbWidth, uint32_t bbHeight, uint32_t kernelW, uint32_t kernelH);
	void    DestroyResource();
	void	ExecuteReduction(ComPtr<ID3D11ShaderResourceView> inputSRV);
	vector<vector<float>> GetResult();

	ID3D11ShaderResourceView** GetFNNResulttSRV() { return m_pSRVVec.back().GetAddressOf(); }

private:
	ComPtr<ID3D11VertexShader>		m_pVertexShader;
	ComPtr<ID3D11GeometryShader>	m_pGeometryShader;
	ComPtr<ID3D11PixelShader>		m_pPixelShader;
	ComPtr<ID3D11Buffer>			m_pVertexBuffer;
	ComPtr<ID3D11Buffer>			m_pIndexBuffer;
	ComPtr<ID3D11InputLayout>		m_pVertexLayout;
	ComPtr<ID3D11RasterizerState>	m_pSolidStateRS;

	ComPtr<ID3D11Buffer>			m_pFreqUpdateBuffer;

	vector<ComPtr<ID3D11Texture2D>>				m_pTex2DVec;
	vector<ComPtr<ID3D11ShaderResourceView>>	m_pSRVVec;
	vector<ComPtr<ID3D11RenderTargetView>>		m_pRTVVec;
	ComPtr<ID3D11Texture2D>						m_pStagingTex;

	ComPtr<ID3D11Texture2D>			m_pDepthTex2D;
	ComPtr<ID3D11DepthStencilState> m_pDepthStencilState;
	ComPtr<ID3D11DepthStencilView>	m_pDepthStencilView;

	vector<D3D11_VIEWPORT>			m_ViewPort;

	uint32_t					m_bbWidth;
	uint32_t					m_bbHeight;
	uint32_t					m_kernelW;
	uint32_t					m_kernelH;

public:
	// Test 
	ComPtr<ID3D11Texture2D>	m_pTestTex2Ds;
	ComPtr<ID3D11ShaderResourceView> m_pTestSRVs;

private:
	struct FreqUpdatedCB
	{
		uint32_t kernelW;
		uint32_t kernelH;
		uint32_t currentW;
		uint32_t currentH;
	};

	struct BGVertex {
		XMFLOAT3 Pos;
		XMFLOAT2 Tex;
	};
};
