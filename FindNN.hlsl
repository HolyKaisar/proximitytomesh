
//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
SamplerState samLinear : register(s0);

cbuffer cbChangesEveryFrame : register(b0)
{
	matrix WorldViewProj;
	matrix World;
	float4 CtrPt;
};

//--------------------------------------------------------------------------------------
struct VS_INPUT
{
	float3 Pos : POSITION;
	float3 Nor : NORMAL;
	float2 Tex : TEXCOORD;
};

struct PS_INPUT
{
	float4 Pos : SV_POSITION;
	float4 ScreenPos: TEXCOORD0;
	float2 Dis : TEXCOORD1;
};


//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
PS_INPUT VS(VS_INPUT input, uint id: SV_VertexID)
{
	PS_INPUT output = (PS_INPUT)0;
	output.Pos = mul(float4(input.Pos, 1.0f), WorldViewProj);
	output.ScreenPos = output.Pos;

	float4 worldPos = float4(input.Pos, 0.0f);
	float dis = length(worldPos - CtrPt);
	output.Dis = float2(dis, id);

	return output;
}


//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(PS_INPUT input) : SV_Target
{
	float4 res = float4(1.0f, 1.0f, 1.0f, 1.0f);
	float2 screenPos = input.ScreenPos.xy / input.ScreenPos.w;
	res.xy = screenPos;
	res.z = input.Dis.x / 10000.0f;

	res.x = res.z;
	res.y = res.z;
	res.w = input.Dis.y;
	return res;
}
