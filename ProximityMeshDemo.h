#pragma once

#include "GHeader.h"
#include "Model.h"
#include "TexProjectionEffect.h"
#include "BasicEffect.h"
#include "DepthMapEffect.h"

class ProximityMeshDemo
{
public:
	ProximityMeshDemo();
	ProximityMeshDemo(const ProximityMeshDemo&) = delete;
	~ProximityMeshDemo();

	// DX11 Resources
	HRESULT CreateResource(uint32_t bbWidth, uint32_t bbHeight);
	void	DestroyResource();
	void	Render();
	void	UpdateCamera(float timeElapsed);

	void	RenderGUI();

	LRESULT HandleMessages(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		m_modelViewCamera.HandleMessages(hWnd, uMsg, wParam, lParam);
		return 0;
	}

	HRESULT LoadModel(string model_file_path)
	{
		HRESULT hr = S_OK;
		m_model.Destroy();
		V_RETURN(m_model.Create(model_file_path, "ConvexObj"));
		return hr;
	}

	void PointTranslation(XMMATRIX translation)
	{
		m_sphere.SetTranslation(translation);
	}

	ID3D11ShaderResourceView** GetDemoResultSRV() { return m_pSRV.GetAddressOf(); }
	//ID3D11ShaderResourceView** GetFNNResultSRV() { return m_fnn.GetFNNResulttSRV(); }
private:
	CModelViewerCamera			m_modelViewCamera;

	ComPtr<ID3D11Texture2D>				m_pTex2D;
	ComPtr<ID3D11ShaderResourceView>	m_pSRV;
	ComPtr<ID3D11RenderTargetView>		m_pRTV;

	ComPtr<ID3D11Texture2D>				m_pDepthTex2D;
	ComPtr<ID3D11DepthStencilState>		m_pDepthStencilState;
	ComPtr<ID3D11DepthStencilView>		m_pDepthStencilView;

	ComPtr<ID3D11RasterizerState>		m_pSolidStateRS;
	ComPtr<ID3D11BlendState>			m_pTransparentBS;

	XMMATRIX						m_WorldMatrix;

	D3D11_VIEWPORT					m_demoViewPort;

	uint32_t						m_bbWidth;
	uint32_t						m_bbHeight;
private:
	ptmDemo::Model m_model;
	ptmDemo::Model m_sphere;

	TexProjectionEffect m_tpEffect;
	BasicEffect m_bEffect;
	DepthMapEffect m_dmEffect;
};

