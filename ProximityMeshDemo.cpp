#include "ProximityMeshDemo.h"

ProximityMeshDemo::ProximityMeshDemo()
{
	m_pTex2D = nullptr;
	m_pSRV = nullptr;
	m_pRTV = nullptr;

	m_pDepthTex2D = nullptr;
	m_pDepthStencilState = nullptr;
	m_pDepthStencilView = nullptr;

	m_pSolidStateRS = nullptr;
	m_pTransparentBS = nullptr;

	m_bbWidth = m_bbHeight = 1;

}

ProximityMeshDemo::~ProximityMeshDemo()
{
	DestroyResource();
}

void ProximityMeshDemo::DestroyResource()
{
	m_pTex2D.Reset();
	m_pSRV.Reset();
	m_pRTV.Reset();

	m_pDepthTex2D.Reset();
	m_pDepthStencilState.Reset();
	m_pDepthStencilView.Reset();

	m_pSolidStateRS.Reset();
	m_pTransparentBS.Reset();

	m_model.Destroy();
	m_sphere.Destroy();

	m_tpEffect.DestroyResource();
	m_bEffect.DestroyResource();
	m_dmEffect.DestroyResource();
}

HRESULT ProximityMeshDemo::CreateResource(uint32_t bbWidth, uint32_t bbHeight)
{
	HRESULT hr = S_OK;
	ID3D11Device* pd3dDevice = DXUTGetD3D11Device();

	DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;

#ifdef _DEBUG
	// Set the D3DCOMPILE_DEBUG flag to embed debug information in the shaders.
	// Setting this flag improves the shader debugging experience, but still allows 
	// the shaders to be optimized and to run exactly the way they will run in 
	// the release configuration of this program.
	dwShaderFlags |= D3DCOMPILE_DEBUG;

	// Disable optimizations to further improve shader debugging
	dwShaderFlags |= D3DCOMPILE_SKIP_OPTIMIZATION;
#endif

	// Initialize the world matrices
	m_WorldMatrix = XMMatrixIdentity();

	// Setup the camera's view parameters
	static const XMVECTORF32 s_Eye = { 0.0f, 0.0f, -6.0f, 0.f };
	static const XMVECTORF32 s_At = { 0.0f, 0.0f, 0.0f, 0.f };
	m_modelViewCamera.SetViewParams(s_Eye, s_At);
	// Setup the camera's projection parameters
	float fAspectRatio = bbWidth / (float)bbHeight;
	m_modelViewCamera.SetProjParams(XM_PI / 4, fAspectRatio, 0.01f, 1000.0f);
	m_modelViewCamera.SetWindow(bbWidth, bbHeight);
	m_modelViewCamera.SetButtonMasks(MOUSE_LEFT_BUTTON, MOUSE_WHEEL, MOUSE_MIDDLE_BUTTON);

	// Create texture resources
	D3D11_TEXTURE2D_DESC	RTtextureDesc = { 0 };
	RTtextureDesc.Width = bbWidth;
	RTtextureDesc.Height = bbHeight;
	RTtextureDesc.MipLevels = 1;
	RTtextureDesc.ArraySize = 1;
	RTtextureDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	RTtextureDesc.SampleDesc.Count = 1;
	RTtextureDesc.Usage = D3D11_USAGE_DEFAULT;
	RTtextureDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	RTtextureDesc.CPUAccessFlags = 0;
	RTtextureDesc.MiscFlags = 0;
	V_RETURN(pd3dDevice->CreateTexture2D(&RTtextureDesc, NULL, m_pTex2D.GetAddressOf()));

	D3D11_SHADER_RESOURCE_VIEW_DESC RTshaderResourceDesc;
	RTshaderResourceDesc.Format = RTtextureDesc.Format;
	RTshaderResourceDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	RTshaderResourceDesc.Texture2D.MostDetailedMip = 0;
	RTshaderResourceDesc.Texture2D.MipLevels = 1;
	V_RETURN(pd3dDevice->CreateShaderResourceView(m_pTex2D.Get(), &RTshaderResourceDesc, m_pSRV.GetAddressOf()));

	D3D11_RENDER_TARGET_VIEW_DESC	RTviewDesc;
	RTviewDesc.Format = RTtextureDesc.Format;
	RTviewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	RTviewDesc.Texture2D.MipSlice = 0;
	V_RETURN(pd3dDevice->CreateRenderTargetView(m_pTex2D.Get(), &RTviewDesc, m_pRTV.GetAddressOf()));

	// Create Stencil View
	D3D11_TEXTURE2D_DESC descDepth;
	ZeroMemory(&descDepth, sizeof(descDepth));
	descDepth.Width = bbWidth;
	descDepth.Height = bbHeight;
	descDepth.MipLevels = 1;
	descDepth.ArraySize = 1;
	descDepth.Format = DXUTGetDeviceSettings().d3d11.AutoDepthStencilFormat;
	descDepth.SampleDesc.Count = 1;
	descDepth.SampleDesc.Quality = 0;
	descDepth.Usage = D3D11_USAGE_DEFAULT;
	descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	descDepth.CPUAccessFlags = 0;
	descDepth.MiscFlags = 0;
	hr = pd3dDevice->CreateTexture2D(&descDepth, NULL, m_pDepthTex2D.GetAddressOf());

	D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
	ZeroMemory(&descDSV, sizeof(descDSV));
	descDSV.Format = descDepth.Format;
	descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	descDSV.Texture2D.MipSlice = 0;

	// Create Depth Stencil State
	D3D11_DEPTH_STENCIL_DESC dsDesc;
	// Depth test parameters
	dsDesc.DepthEnable = false; // Disable Depth Test
	dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	//dsDesc.DepthFunc = D3D11_COMPARISON_LESS;
	dsDesc.DepthFunc = D3D11_COMPARISON_LESS;
	// Stencil test parameters
	dsDesc.StencilEnable = true;
	dsDesc.StencilReadMask = 0xFF;
	dsDesc.StencilWriteMask = 0xFF;
	// Stencil operations if pixel is front-facing
	dsDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	dsDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	// Stencil operations if pixel is back-facing
	dsDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	dsDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	V_RETURN(pd3dDevice->CreateDepthStencilState(&dsDesc, m_pDepthStencilState.GetAddressOf()));

	// Create the depth stencil view
	V_RETURN(pd3dDevice->CreateDepthStencilView(m_pDepthTex2D.Get(), // Depth stencil texture
		&descDSV, // Depth stencil desc
		m_pDepthStencilView.GetAddressOf()));

	// Setup ViewPort
	m_demoViewPort.TopLeftX = 0;
	m_demoViewPort.TopLeftY = 0;
	m_demoViewPort.MinDepth = 0.0f;
	m_demoViewPort.MaxDepth = 1.0f;
	m_demoViewPort.Width = static_cast<float>(bbWidth);
	m_demoViewPort.Height = static_cast<float>(bbHeight);


	D3D11_RASTERIZER_DESC rasterizerState;
	rasterizerState.FillMode = D3D11_FILL_SOLID;
	rasterizerState.CullMode = D3D11_CULL_NONE;
	rasterizerState.FrontCounterClockwise = true;
	rasterizerState.DepthBias = false;
	rasterizerState.DepthBiasClamp = 0;
	rasterizerState.SlopeScaledDepthBias = 0;
	rasterizerState.DepthClipEnable = true;
	rasterizerState.ScissorEnable = false;
	rasterizerState.MultisampleEnable = true;
	rasterizerState.AntialiasedLineEnable = false;
	V_RETURN(pd3dDevice->CreateRasterizerState(&rasterizerState, m_pSolidStateRS.GetAddressOf()));
	
	D3D11_BLEND_DESC blendDesc;
	ZeroMemory(&blendDesc, sizeof(D3D11_BLEND_DESC));
	// Create an alpha enabled blend state description.
	blendDesc.RenderTarget[0].BlendEnable = true;
	blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
	blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].RenderTargetWriteMask = 0x0f;
	V_RETURN(pd3dDevice->CreateBlendState(&blendDesc, m_pTransparentBS.GetAddressOf()));

	// Load target model
	m_model.Create("concave_shape_ascii.off", "ConvexObj");
	//m_model.Create("TestOff.off", "ProximityMeshDemo.hlsl", "ConvexObj");

	// Load sphere model
	m_sphere.Create("sphere.obj", "Sphere");
	XMMATRIX transform = XMMatrixTranslation(100.0, 0.0, 0.0);
	m_sphere.SetTransform(transform);

	// Create texture projection effect
	m_tpEffect.CreateResource(bbWidth, bbHeight);
	m_bEffect.CreateResource(bbWidth, bbHeight);
	m_dmEffect.CreateResource(bbWidth, bbHeight);

	m_bbWidth = bbWidth;
	m_bbHeight = bbHeight;
	return hr;
}

void ProximityMeshDemo::Render()
{
	ID3D11DeviceContext* pd3dImmediateContext = DXUTGetD3D11DeviceContext();

	// Find Nearest Neighbor
	XMFLOAT4 ctrTrans = m_sphere.GetTranslation();
	// Find the nearest point on the GPU end
	XMFLOAT3 nnPt = m_model.FindNearestNeighborForMesh(0, XMFLOAT3(ctrTrans.x, ctrTrans.y, ctrTrans.z));
	// Projective Matrix (view and proj)
	XMVECTOR up = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);
	XMVECTOR eye = XMVectorSet(ctrTrans.x, ctrTrans.y, ctrTrans.z, 0.0f);
	XMVECTOR at = XMVectorSet(nnPt.x, nnPt.y, nnPt.z, 0.0f);
	XMMATRIX pView = XMMatrixLookAtLH(eye, at, up);
	float fAspectRatio = (float)m_bbWidth / (float)m_bbHeight;
	XMMATRIX pProj = XMMatrixPerspectiveFovLH(XM_PI / 4, fAspectRatio, 0.01f, 1000.0f);

	XMMATRIX view = m_modelViewCamera.GetViewMatrix(), proj = m_modelViewCamera.GetProjMatrix();


	// Get Depth map of the object
	{
		auto [vertexBuffer, indexBuffer, vertexStride, vertexOffset, idxCount, vertexCount] = m_model.GetMeshVertexInfo(0);
		m_dmEffect.RenderDepthMap(vertexBuffer, indexBuffer, idxCount, vertexStride, vertexOffset, XMMatrixIdentity(), pView, pProj);
	}

	ID3D11RasterizerState* rs = nullptr;
	pd3dImmediateContext->RSGetState(&rs);

	//pd3dImmediateContext->ClearRenderTargetView(m_pRTV.Get(), Colors::MidnightBlue);
	pd3dImmediateContext->ClearRenderTargetView(m_pRTV.Get(), Colors::Black);
	pd3dImmediateContext->ClearDepthStencilView(m_pDepthStencilView.Get(), D3D11_CLEAR_DEPTH, 1.0, 0);
	pd3dImmediateContext->OMSetRenderTargets(1, m_pRTV.GetAddressOf(), m_pDepthStencilView.Get());
	pd3dImmediateContext->OMSetDepthStencilState(m_pDepthStencilState.Get(), 1);

	pd3dImmediateContext->RSSetViewports(1, &m_demoViewPort);
	pd3dImmediateContext->RSSetState(m_pSolidStateRS.Get());


	// Render Convex Object
	{
		float blendFactor[4] = { 0, 0, 0, 0 };
		pd3dImmediateContext->OMSetBlendState(m_pTransparentBS.Get(), blendFactor, 0xffffffff);
		auto [vertexBuffer, indexBuffer, vertexStride, vertexOffset, idxCount, vertexCount] = m_model.GetMeshVertexInfo(0);
		XMMATRIX objTransform = m_model.GetTranform();
		m_tpEffect.Render(vertexBuffer, indexBuffer, idxCount, vertexStride, vertexOffset, 
			objTransform * m_WorldMatrix, view, proj, pView, pProj, ctrTrans, m_dmEffect.GetDepthMap());
		pd3dImmediateContext->OMSetBlendState(0, 0, 0xffffffff);
	}
	// Render Sphere
	{
		auto [vertexBuffer, indexBuffer, vertexStride, vertexOffset, idxCount, vertexCount] = m_sphere.GetMeshVertexInfo(0);
		XMMATRIX objTransform = m_sphere.GetTranform();
		m_bEffect.Render(vertexBuffer, indexBuffer, idxCount, vertexStride, vertexOffset, objTransform * m_WorldMatrix, view, proj);
	}

	ID3D11VertexShader* tmpVertexShader = nullptr;
	ID3D11PixelShader* tmpPixelShader = nullptr;
	ID3D11Buffer* tmpBuffer = nullptr;
	ID3D11ShaderResourceView* tmpSRV = nullptr;
	ID3D11SamplerState* tmpSampler = nullptr;
	pd3dImmediateContext->VSSetShader(tmpVertexShader, nullptr, 0);
	pd3dImmediateContext->VSSetConstantBuffers(0, 1, &tmpBuffer);
	pd3dImmediateContext->PSSetShader(tmpPixelShader, nullptr, 0);
	pd3dImmediateContext->PSSetConstantBuffers(0, 1, &tmpBuffer);
	pd3dImmediateContext->PSSetShaderResources(0, 1, &tmpSRV);
	pd3dImmediateContext->PSSetSamplers(0, 1, &tmpSampler);

	// Restore the blend state
	pd3dImmediateContext->OMSetBlendState(0, 0, 0xffffffff);
	pd3dImmediateContext->RSSetState(rs);
	SAFE_RELEASE(rs);
}

void ProximityMeshDemo::UpdateCamera(float timeElapsed)
{
	// Update the camera's position based on user input 
	m_modelViewCamera.FrameMove(timeElapsed);
}

//*******************************
// GUI Implementation
//*******************************
void ProximityMeshDemo::RenderGUI()
{
	if (ImGui::CollapsingHeader("Proximity To Mesh Controller"))
	{
		ImGui::BulletText("Nearest Neighbor Lookup Method (Need a Test)");
		//ImGui::Indent();
		ImGui::Separator();
		enum Mode
		{
			One,
			Two,
			Four,
			Eight
		};
		static int mode = 0;
		if (ImGui::RadioButton("CPU Method", mode == One))
		{
			mode = One;
		}
		ImGui::SameLine();
		if (ImGui::RadioButton("GPU Method", mode == Two))
		{
			mode = Two;
		}
		//ImGui::SameLine();
		//if (ImGui::RadioButton("Four", mode == Four))
		//{
		//	mode = Four;
		//}
		//ImGui::SameLine();
		//if (ImGui::RadioButton("Eight", mode == Eight))
		//{
		//	mode = Eight;
		//}

		//ImGui::Separator();

		//ImGui::BulletText("MSAA Sampling Quality");
		//ImGui::Separator();
		//static float msaaSampleQuality = 0.5f;
		//ImGui::SliderFloat("Quality Value", &msaaSampleQuality, 0.0f, 1.0f, "Quality Value = %.3f");
		ImGui::Separator();
	}
}
