#pragma once

// DXUT headers
#include "DXUT.h"
#include "DXUTgui.h"
#include "DXUTmisc.h"
#include "DXUTCamera.h"
#include "DXUTSettingsDlg.h"
#include "SDKmisc.h"
#include "SDKmesh.h"
#include "resource.h"

// C++ headers
#include <iostream>
#include <algorithm>
#include <limits.h>
#include <vector>
#include <stack>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <fstream>
#include <sstream>
#include <cstring>
#include <cmath>

// IMGUI headers
#include "imgui.h"
#include "imgui_impl_win32.h"
#include "imgui_impl_dx11.h"

// Windows headers
#include <wrl.h>

using namespace std;
using namespace DirectX;
using namespace Microsoft::WRL;