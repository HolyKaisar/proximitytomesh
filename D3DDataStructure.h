#pragma once

#include <DirectXMath.h>

namespace ptmDemo
{
	using namespace DirectX;

	struct D3DVertex
	{
		XMFLOAT3 mPos;
		XMFLOAT3 mNormal;
		XMFLOAT2 mTex;
	};
}