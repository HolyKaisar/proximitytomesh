#include "GPUTexReduction.h"

GPUTexReduction::GPUTexReduction()
{
	m_pVertexShader = nullptr;
	m_pGeometryShader = nullptr;
	m_pPixelShader = nullptr;
	m_pVertexLayout = nullptr;
	m_pSolidStateRS = nullptr;

	m_pFreqUpdateBuffer = nullptr;

	m_pDepthTex2D = nullptr;
	m_pDepthStencilState = nullptr;
	m_pDepthStencilView = nullptr;
}

GPUTexReduction::~GPUTexReduction()
{
	DestroyResource();
}

HRESULT GPUTexReduction::CreateResource(ID3D11Device* pd3dDevice, uint32_t bbWidth, uint32_t bbHeight, uint32_t kernelW, uint32_t kernelH)
{
	HRESULT hr = S_OK;

	DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;

#ifdef _DEBUG
	// Set the D3DCOMPILE_DEBUG flag to embed debug information in the shaders.
	// Setting this flag improves the shader debugging experience, but still allows 
	// the shaders to be optimized and to run exactly the way they will run in 
	// the release configuration of this program.
	dwShaderFlags |= D3DCOMPILE_DEBUG;

	// Disable optimizations to further improve shader debugging
	dwShaderFlags |= D3DCOMPILE_SKIP_OPTIMIZATION;
#endif

	// Compile the vertex shader
	ID3DBlob* pVSBlob = nullptr;
	V_RETURN(DXUTCompileFromFile(L"GPUReduction.hlsl", nullptr, "VS", "vs_5_0", dwShaderFlags, 0, &pVSBlob));

	// Create the vertex shader
	hr = pd3dDevice->CreateVertexShader(pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), nullptr, m_pVertexShader.GetAddressOf());
	if (FAILED(hr))
	{
		SAFE_RELEASE(pVSBlob);
		return hr;
	}

	// Define the input layout
	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT , D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	UINT numElements = ARRAYSIZE(layout);

	// Create the input layout
	hr = pd3dDevice->CreateInputLayout(layout, numElements, pVSBlob->GetBufferPointer(),
		pVSBlob->GetBufferSize(), &m_pVertexLayout);
	SAFE_RELEASE(pVSBlob);
	if (FAILED(hr))
		return hr;


	// Compile the pixel shader
	ID3DBlob* pPSBlob = nullptr;
	V_RETURN(DXUTCompileFromFile(L"GPUReduction.hlsl", nullptr, "PS", "ps_5_0", dwShaderFlags, 0, &pPSBlob));

	// Create the pixel shader
	hr = pd3dDevice->CreatePixelShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), nullptr, &m_pPixelShader);
	SAFE_RELEASE(pPSBlob);
	if (FAILED(hr))
		return hr;


	BGVertex vertices[] = {
		{ XMFLOAT3(-1.0f, -1.0f, 0.0f), XMFLOAT2(0.0f, 1.0f) },
		{ XMFLOAT3(1.0f, -1.0f, 0.0f), XMFLOAT2(1.0f, 1.0f) },
		{ XMFLOAT3(1.0f, 1.0f, 0.0f), XMFLOAT2(1.0f, 0.0f) },
		{ XMFLOAT3(-1.0f, 1.0f, 0.0f), XMFLOAT2(0.0f, 0.0f) },
	};

	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(BGVertex) * 4;
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;
	D3D11_SUBRESOURCE_DATA InitData;
	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = vertices;
	V_RETURN(pd3dDevice->CreateBuffer(&bd, &InitData, &m_pVertexBuffer));

	/*
	* Create Index Buffer
	*/
	DWORD indices[] = {
		3,1,0,
		2,1,3,
	};

	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(DWORD) * 6;
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;
	bd.MiscFlags = 0;
	InitData.pSysMem = indices;
	V_RETURN(pd3dDevice->CreateBuffer(&bd, &InitData, &m_pIndexBuffer));

	// Create the constant buffers
	ZeroMemory(&bd, sizeof(bd));
	//bd.Usage = D3D11_USAGE_DYNAMIC;
	//bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	//bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	//bd.ByteWidth = sizeof(FreqUpdatedCB);
	//V_RETURN(pd3dDevice->CreateBuffer(&bd, nullptr, m_pFreqUpdateBuffer.GetAddressOf()));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;
	bd.ByteWidth = sizeof(FreqUpdatedCB);
	V_RETURN(pd3dDevice->CreateBuffer(&bd, nullptr, m_pFreqUpdateBuffer.GetAddressOf()));

	//// Create the sample state
	//D3D11_SAMPLER_DESC sampDesc;
	//ZeroMemory(&sampDesc, sizeof(sampDesc));
	//sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	//sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	//sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	//sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	//sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	//sampDesc.MinLOD = 0;
	//sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
	//V_RETURN(pd3dDevice->CreateSamplerState(&sampDesc, m_pSamplerLinear.GetAddressOf()));

	// Create Stencil View
	D3D11_TEXTURE2D_DESC descDepth;
	ZeroMemory(&descDepth, sizeof(descDepth));
	descDepth.Width = bbWidth;
	descDepth.Height = bbHeight;
	descDepth.MipLevels = 1;
	descDepth.ArraySize = 1;
	descDepth.Format = DXUTGetDeviceSettings().d3d11.AutoDepthStencilFormat;
	descDepth.SampleDesc.Count = 1;
	descDepth.SampleDesc.Quality = 0;
	descDepth.Usage = D3D11_USAGE_DEFAULT;
	descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	descDepth.CPUAccessFlags = 0;
	descDepth.MiscFlags = 0;
	hr = pd3dDevice->CreateTexture2D(&descDepth, NULL, m_pDepthTex2D.GetAddressOf());

	D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
	ZeroMemory(&descDSV, sizeof(descDSV));
	descDSV.Format = descDepth.Format;
	descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	descDSV.Texture2D.MipSlice = 0;

	// Create Depth Stencil State
	D3D11_DEPTH_STENCIL_DESC dsDesc;
	// Depth test parameters
	dsDesc.DepthEnable = true; // Disable Depth Test
	dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	dsDesc.DepthFunc = D3D11_COMPARISON_LESS_EQUAL;
	// Stencil test parameters
	dsDesc.StencilEnable = true;
	dsDesc.StencilReadMask = 0xFF;
	dsDesc.StencilWriteMask = 0xFF;
	// Stencil operations if pixel is front-facing
	dsDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	dsDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	// Stencil operations if pixel is back-facing
	dsDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	dsDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	V_RETURN(pd3dDevice->CreateDepthStencilState(&dsDesc, m_pDepthStencilState.GetAddressOf()));

	// Create the depth stencil view
	V_RETURN(pd3dDevice->CreateDepthStencilView(m_pDepthTex2D.Get(), // Depth stencil texture
		&descDSV, // Depth stencil desc
		m_pDepthStencilView.GetAddressOf()));

	D3D11_RASTERIZER_DESC rasterizerState;
	rasterizerState.FillMode = D3D11_FILL_SOLID;
	rasterizerState.CullMode = D3D11_CULL_NONE;
	rasterizerState.FrontCounterClockwise = true;
	rasterizerState.DepthBias = false;
	rasterizerState.DepthBiasClamp = 0;
	rasterizerState.SlopeScaledDepthBias = 0;
	rasterizerState.DepthClipEnable = true;
	rasterizerState.ScissorEnable = false;
	rasterizerState.MultisampleEnable = true;
	rasterizerState.AntialiasedLineEnable = false;
	V_RETURN(pd3dDevice->CreateRasterizerState(&rasterizerState, m_pSolidStateRS.GetAddressOf()));

	m_bbWidth = bbWidth;
	m_bbHeight = bbHeight;

	m_kernelW = kernelW;
	m_kernelH = kernelH;

	int iterCnt = min(log(m_bbWidth) / log(4), log(m_bbHeight) / log(4));
	int tmpW = static_cast<int>(m_bbWidth), tmpH = static_cast<int>(m_bbHeight);

	for (int i = 0; i < iterCnt - 1; i++)
	{
		tmpW = ceil(1.0 * tmpW / kernelW);
		tmpH = ceil(1.0 * tmpH / kernelH);
		ComPtr<ID3D11Texture2D>				pTex;
		ComPtr<ID3D11ShaderResourceView>	pSRV;
		ComPtr<ID3D11RenderTargetView>		pRTV;
		D3D11_VIEWPORT						vp;

		// Create texture resources
		D3D11_TEXTURE2D_DESC	RTtextureDesc = { 0 };
		RTtextureDesc.Width = tmpW;
		RTtextureDesc.Height = tmpH;
		RTtextureDesc.MipLevels = 1;
		RTtextureDesc.ArraySize = 1;
		RTtextureDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
		RTtextureDesc.SampleDesc.Count = 1;
		RTtextureDesc.Usage = D3D11_USAGE_DEFAULT;
		RTtextureDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
		RTtextureDesc.CPUAccessFlags = 0;
		RTtextureDesc.MiscFlags = 0;
		V_RETURN(pd3dDevice->CreateTexture2D(&RTtextureDesc, NULL, pTex.GetAddressOf()));

		D3D11_SHADER_RESOURCE_VIEW_DESC RTshaderResourceDesc;
		RTshaderResourceDesc.Format = RTtextureDesc.Format;
		RTshaderResourceDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		RTshaderResourceDesc.Texture2D.MostDetailedMip = 0;
		RTshaderResourceDesc.Texture2D.MipLevels = 1;
		V_RETURN(pd3dDevice->CreateShaderResourceView(pTex.Get(), &RTshaderResourceDesc, pSRV.GetAddressOf()));

		D3D11_RENDER_TARGET_VIEW_DESC	RTviewDesc;
		RTviewDesc.Format = RTtextureDesc.Format;
		RTviewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
		RTviewDesc.Texture2D.MipSlice = 0;
		V_RETURN(pd3dDevice->CreateRenderTargetView(pTex.Get(), &RTviewDesc, pRTV.GetAddressOf()));

		vp.Width = tmpW;
		vp.Height = tmpH;
		vp.MinDepth = 0;
		vp.MaxDepth = 1;
		vp.TopLeftX = 0;
		vp.TopLeftY = 0;

		m_pTex2DVec.push_back(pTex);
		m_pSRVVec.push_back(pSRV);
		m_pRTVVec.push_back(pRTV);
		m_ViewPort.push_back(vp);
	}

	D3D11_TEXTURE2D_DESC	RTtextureDesc = { 0 };
	RTtextureDesc.Width = tmpW;
	RTtextureDesc.Height = tmpH;
	RTtextureDesc.MipLevels = 1;
	RTtextureDesc.ArraySize = 1;
	RTtextureDesc.SampleDesc.Count = 1;
	RTtextureDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	RTtextureDesc.Usage = D3D11_USAGE_STAGING;
	RTtextureDesc.BindFlags = 0;
	RTtextureDesc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
	V_RETURN(pd3dDevice->CreateTexture2D(&RTtextureDesc, nullptr, &m_pStagingTex));

	D3D11_VIEWPORT vp;
	vp.Width = tmpW;
	vp.Height = tmpH;
	vp.MinDepth = 0;
	vp.MaxDepth = 1;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	m_ViewPort.push_back(vp);


	// TODO TestCode
	{
		D3D11_TEXTURE2D_DESC	td = { 0 };
		td.Width = m_bbWidth;
		td.Height = m_bbHeight;
		td.MipLevels = 1;
		td.ArraySize = 1;
		td.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
		td.SampleDesc.Count = 1;
		td.Usage = D3D11_USAGE_DEFAULT;
		td.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET;
		td.CPUAccessFlags = 0;
		td.MiscFlags = 0;

		float* dummyData = new float[m_bbWidth * m_bbHeight * 4];
		//for (int i = 0; i < m_bbWidth * m_bbHeight * 4; i++) dummyData[i] = 1.0f;
		float cnt = 1.0f;
		for (int i = 0; i < m_bbWidth * m_bbHeight * 4; i += 4)
		{
			dummyData[i] = cnt;
			dummyData[i + 1] = cnt;
			dummyData[i + 2] = cnt;
			dummyData[i + 3] = cnt;
			cnt += 1.0f;
		}
		D3D11_SUBRESOURCE_DATA subData;
		subData.pSysMem = dummyData;
		subData.SysMemPitch = sizeof(float) * m_bbWidth * 4;
		V_RETURN(pd3dDevice->CreateTexture2D(&td, &subData, m_pTestTex2Ds.GetAddressOf()));
		V_RETURN(pd3dDevice->CreateShaderResourceView(m_pTestTex2Ds.Get(), nullptr, m_pTestSRVs.GetAddressOf()));
		delete[] dummyData;
	}

	return hr;
}

void GPUTexReduction::DestroyResource()
{
	m_pVertexShader.Reset();
	m_pGeometryShader.Reset();
	m_pPixelShader.Reset();
	m_pVertexBuffer.Reset();
	m_pIndexBuffer.Reset();
	m_pVertexLayout.Reset();
	m_pSolidStateRS.Reset();

	m_pFreqUpdateBuffer.Reset();

	for (auto& ptr : m_pRTVVec)
		ptr.Reset();
	for (auto& ptr : m_pSRVVec)
		ptr.Reset();
	for (auto& ptr : m_pTex2DVec)
		ptr.Reset();

	m_pRTVVec.clear();
	m_pSRVVec.clear();
	m_pTex2DVec.clear();

	m_pStagingTex.Reset();

	m_pDepthTex2D.Reset();
	m_pDepthStencilState.Reset();
	m_pDepthStencilView.Reset();

	// Test Code
	m_pTestTex2Ds.Reset();
	m_pTestSRVs.Reset();
}

void GPUTexReduction::ExecuteReduction(ComPtr<ID3D11ShaderResourceView> inputSRV)
{
	ID3D11DeviceContext* pd3dImmediateContext = DXUTGetD3D11DeviceContext();

	ID3D11RasterizerState* rs = nullptr;
	pd3dImmediateContext->RSGetState(&rs);

	pd3dImmediateContext->VSSetShader(NULL, NULL, 0);
	pd3dImmediateContext->GSSetShader(NULL, NULL, 0);
	pd3dImmediateContext->PSSetShader(NULL, NULL, 0);

	pd3dImmediateContext->IASetInputLayout(m_pVertexLayout.Get());

	UINT stride = sizeof(BGVertex);
	UINT offset = 0;
	pd3dImmediateContext->IASetVertexBuffers(0, 1, m_pVertexBuffer.GetAddressOf(), &stride, &offset);
	pd3dImmediateContext->IASetIndexBuffer(m_pIndexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);
	pd3dImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	for (int i = 0; i < m_pTex2DVec.size(); i++)
	{
		float ClearColor[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
		pd3dImmediateContext->OMSetRenderTargets(1, m_pRTVVec[i].GetAddressOf(), NULL);
		pd3dImmediateContext->ClearRenderTargetView(m_pRTVVec[i].Get(), ClearColor);
		pd3dImmediateContext->RSSetViewports(1, &m_ViewPort[i]);

		pd3dImmediateContext->RSSetState(m_pSolidStateRS.Get());
		pd3dImmediateContext->VSSetShader(m_pVertexShader.Get(), NULL, 0);
		if (i == 0) pd3dImmediateContext->PSSetShaderResources(0, 1, inputSRV.GetAddressOf());
		else pd3dImmediateContext->PSSetShaderResources(0, 1, m_pSRVVec[i - 1].GetAddressOf());
		pd3dImmediateContext->PSSetShader(m_pPixelShader.Get(), NULL, 0);


		FreqUpdatedCB CBDynamic;
		CBDynamic.kernelW = m_kernelW;
		CBDynamic.kernelH = m_kernelH;
		D3D11_TEXTURE2D_DESC	RTtextureDesc = { 0 };
		m_pTex2DVec[i]->GetDesc(&RTtextureDesc);
		CBDynamic.currentW = RTtextureDesc.Width;
		CBDynamic.currentH = RTtextureDesc.Height;

		pd3dImmediateContext->UpdateSubresource(m_pFreqUpdateBuffer.Get(), 0, NULL, &CBDynamic, 0, 0);




		pd3dImmediateContext->PSSetConstantBuffers(0, 1, m_pFreqUpdateBuffer.GetAddressOf());

		pd3dImmediateContext->DrawIndexed(6, 0, 0);

		ID3D11VertexShader* tmpVertexShader = nullptr;
		ID3D11PixelShader* tmpPixelShader = nullptr;
		ID3D11Buffer* tmpBuffer = nullptr;
		ID3D11ShaderResourceView* tmpSRV = nullptr;
		ID3D11SamplerState* tmpSampler = nullptr;
		pd3dImmediateContext->VSSetShader(tmpVertexShader, nullptr, 0);
		pd3dImmediateContext->VSSetConstantBuffers(0, 1, &tmpBuffer);
		pd3dImmediateContext->PSSetShader(tmpPixelShader, nullptr, 0);
		pd3dImmediateContext->PSSetConstantBuffers(0, 1, &tmpBuffer);
		pd3dImmediateContext->PSSetShaderResources(0, 1, &tmpSRV);
		pd3dImmediateContext->PSSetSamplers(0, 1, &tmpSampler);
		SAFE_RELEASE(rs);
	}
}

vector<vector<float>> GPUTexReduction::GetResult()
{
	vector<vector<float>> res;

	ID3D11DeviceContext* pd3dImmediateContext = DXUTGetD3D11DeviceContext();
	pd3dImmediateContext->CopyResource(m_pStagingTex.Get(), m_pTex2DVec.back().Get());

	D3D11_TEXTURE2D_DESC	RTtextureDesc = { 0 };
	m_pTex2DVec.back()->GetDesc(&RTtextureDesc);

	D3D11_MAPPED_SUBRESOURCE mappedResource;
	pd3dImmediateContext->Map(m_pStagingTex.Get(), 0, D3D11_MAP_READ, 0, &mappedResource);
	std::byte const* rowPtr = (std::byte*)mappedResource.pData;
	for (size_t row = 0u; row < RTtextureDesc.Width; ++row) {
		// Take the current row pointer and interpret as sequence of U32
		float const* p = reinterpret_cast<float const*>(rowPtr);
		for (size_t col = 0u; col < RTtextureDesc.Height; ++col) {
			res.push_back({ *p, *(p + 1), *(p + 2) });
			// Advance one U32
			p += 4;
		}
		// Advance the row pointer to the beginning of the next row
		rowPtr += mappedResource.RowPitch;
	}
	pd3dImmediateContext->Unmap(m_pStagingTex.Get(), 0);
	return res;
}

