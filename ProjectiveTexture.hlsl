
//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
Texture2D<float4> inputImg: register(t0);
Texture2D<float4> depthMap: register(t1);
SamplerState samLinear : register(s0);

cbuffer cbChangesEveryFrame : register(b0)
{
	matrix WorldViewProj;
	matrix WorldViewProj2;
	matrix World;
	float4 PointPos;
};

//--------------------------------------------------------------------------------------
struct VS_INPUT
{
	float3 Pos : POSITION;
	float3 Nor : NORMAL;
	float2 Tex : TEXCOORD;
};

struct PS_INPUT
{
	float4 Pos : SV_POSITION;
	float3 Nor : NORMAL;
	float2 Tex : TEXCOORD0;
	float4 ViewPosition : TEXCOORD1;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
PS_INPUT VS(VS_INPUT input, uint id: SV_VertexID)
{
	float4 worldPosition;

	PS_INPUT output = (PS_INPUT)0;
	output.Pos = mul(float4(input.Pos, 1.0f), WorldViewProj);
	output.ViewPosition = mul(float4(input.Pos, 1.0f), WorldViewProj2);

	output.Tex = input.Tex;
	output.Nor = mul(input.Nor, (float3x3)World);
	output.Nor = normalize(output.Nor);

	worldPosition = mul(float4(input.Pos, 1.0f), World);

	return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(PS_INPUT input) : SV_Target
{
	// Set the bias value for fixing the floating point precision issues.
	// Some points on the surface may be removed incorrectly
	float bias = 0.002f;

	float3 lightPos = float3(0.0f, 4.0f, 5.0f);
	//lightPos = vCameraPos.xyz;
	float3 lightDir = float3(0.0f, 0.0f, 0.0f) - lightPos;
	float3 lightD = -lightDir;
	lightD = normalize(lightD);

	float lightIntensity = saturate(dot(normalize(input.Nor), lightD));

	float4 color = float4(0.15f, 0.15f, 0.15f, 1.0f);
	float4 diffuseColor = float4(0.5f, 0.3f, 0.6f, 1.0f);
	if (lightIntensity > 0.0f)
	{
		// Determine the final diffuse color based on the diffuse color and the amount of light intensity.
		color += (diffuseColor * lightIntensity);
	}
	// Saturate the light color.
	color = saturate(color);

	// Calculate the projected texture coordinates.
	float2 projectTexCoord;
	projectTexCoord.x = input.ViewPosition.x / input.ViewPosition.w / 2.0f + 0.5f;
	projectTexCoord.y = -input.ViewPosition.y / input.ViewPosition.w / 2.0f + 0.5f;

	// Determine if the projected coordinates are in the 0 to 1 range.  If it is then this pixel is inside the projected view port.
	if ((saturate(projectTexCoord.x) == projectTexCoord.x) && (saturate(projectTexCoord.y) == projectTexCoord.y))
	{
		float depthValue = depthMap.Sample(samLinear, projectTexCoord).r;
		float lightDepthValue = input.ViewPosition.z;

		lightDepthValue = lightDepthValue - bias;

		if (lightDepthValue < depthValue)
		{
			// Sample the color value from the projection texture using the sampler at the projected texture coordinate location.
			float4 projectionColor = inputImg.Sample(samLinear, projectTexCoord);

			// Set the output color of this pixel to the projection texture overriding the regular color value.
			color = color * projectionColor;
		}
	}
	
	color = saturate(color);
	color.a = 0.1f;
	return color;
}
