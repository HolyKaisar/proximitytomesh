#pragma once

// C++ headers
#include <iostream>
#include <algorithm>
#include <limits.h>
#include <vector>
#include <stack>
#include <queue>
#include <fstream>
#include <cstring>
#include <cmath>
#include <memory>
#include <pdh.h>
#include <assert.h>

#pragma comment(lib, "pdh.lib")


class CPUTimer;

class CPUProfiler
{
public:
private:
};


class CPUInfo
{
public:
	CPUInfo();
	CPUInfo(const CPUInfo&);
	~CPUInfo();

	void Init();
	void Destory();
	void Update();
	int  GetCpuPercentage();

private:
	// CPU Usage Variables
	bool m_canReadCpu;
	HQUERY m_queryHandle;
	HCOUNTER m_counterHandle;
	unsigned long long m_lastSampleTime;
	long long m_cpuUsage;

	// Timers
	std::shared_ptr<CPUTimer> m_cpuTimer;
};


class CPUProfilerGUI
{
public:
	CPUProfilerGUI();
	CPUProfilerGUI(const CPUProfiler&);

};

class CPUTimer
{
public:
	CPUTimer() : m_frequency(0),
		m_ticksPerMs(0),
		m_startTime(0),
		m_frameTime(0)
	{
	}

	CPUTimer(const CPUTimer&) : m_frequency(0),
		m_ticksPerMs(0),
		m_startTime(0),
		m_frameTime(0)
	{
	}

	~CPUTimer() {}

	bool Init()
	{
		// Check if the system supports high performance timers
		QueryPerformanceFrequency((LARGE_INTEGER*)&m_frequency);
		if (m_frequency == 0) return false;

		// Find out how many times the frequency counter ticks every millisecond
		m_ticksPerMs = (float)(m_frequency / 1000);
		QueryPerformanceCounter((LARGE_INTEGER*)&m_startTime);
		return true;
	}

	void Update(float& fps)
	{
		uint64_t currentTime(0);
		float timeDifference(0);

		QueryPerformanceCounter((LARGE_INTEGER*)&currentTime);
		timeDifference = (float)(currentTime - m_startTime);
		m_frameTime = timeDifference / m_ticksPerMs;
		fps = m_frameTime;
	}
private:
	uint64_t m_frequency;
	float m_ticksPerMs;
	uint64_t m_startTime;
	float m_frameTime;
};
